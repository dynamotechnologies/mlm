<?php

class LabelPrinterTest extends MLMTestCase
{
    private $testsubscribers = array();
    private $testlist = NULL;

    /*
     *  Constructor
     */
    public function LabelPrinterTest() {
        $this->init('labelprinter');
    }

    /*
     *  Prepopulate test data
     */
    public function setUp() {
        global $mlm;

        // Create a list to test with
        $lid = $mlm->addList(
            MLMTestData::getData('list', 'topicid1'),
            MLMTestData::getData('list', 'name1'),
            MLMTestData::getData('list', 'unitid1')
        );

        // Create a subscriber to test with
        $sid = $mlm->addSubscriber(
            MLMTestData::getData('subscriber', 'firstname1'),
            MLMTestData::getData('subscriber', 'lastname1'),
            MLMTestData::getData('subscriber', 'salutation1'),
            MLMTestData::getData('subscriber', 'address1_1'),
            MLMTestData::getData('subscriber', 'address2_1'),
            MLMTestData::getData('subscriber', 'city1'),
            MLMTestData::getData('subscriber', 'state1'),
            MLMTestData::getData('subscriber', 'zip1'),
            MLMTestData::getData('subscriber', 'email1'),
            MLMTestData::getData('subscriber', 'phone1'),
            MLMTestData::getData('subscriber', 'country1'),
            MLMTestData::getData('subscriber', 'organization1'),
            MLMTestData::getData('subscriber', 'title1'),
            MLMTestData::getData('subscriber', 'notes1')
        );

        // Retrieve the ORM handles
        $list = $mlm->getList($lid);
        $sub = $mlm->getSubscriber($sid);

        // Keep the id's handy
        $this->testlist = $lid;
        array_push($this->testsubscribers, $sub);

        // Add the test subscriber to the test list
        $mlm->link($sub, $list);
    }


    /*
     *  Clean up test data
     */
    public function tearDown() {
        global $mlm;

        $list = $mlm->getList($this->testlist);
        foreach ($this->testsubscribers as $sub) {
            $mlm->unlink($sub, $list);
            $mlm->delete($sub);
        }
        $mlm->delete($list);
    }


    // ~~~~~~~~~~~~~~~~~~~~~~
    //   Tests
    // ~~~~~~~~~~~~~~~~~~~~~~

    public function test10_PrintLabels() {
        global $mlm;

        // Get the list of subscribers to print
        $subs = $mlm->getSubscribersByList($this->testlist);

        // Print the labels
        $lp = new LabelPrinter();
        $lp->setSubscriberList($subs);
        $lp->printLabels();

        // Make sure everything went okay
        $this->assertTrue($lp->isOk());

        // Get the actual output data for closer examination
        $pdfdata = $lp->getLabelData();

        // For each subscriber in the list...
        foreach ($subs as $s) {
            // Get subscriber's address lines as an array...
            $subarr = explode("\n", $mlm->getSubscriberPrintLabel($s));
            // For each address line...
            foreach ($subarr as $subline) {
                // Confirm that it matches the next line of pdf data.
                $this->assertEqual(array_shift($pdfdata), $subline);
            }
        }

    }

    public function test20_PrintWithNoData() {
        global $mlm;

        // Print the labels with no data
        $subs = array();
        $lp = new LabelPrinter();
        $lp->setSubscriberList($subs);
        $lp->printLabels();

        // Make sure everything went terribly wrong
        $this->assertFalse($lp->isOk());

        // Make sure printer output is as expected
        $stdout = $lp->getStdOut();
        $this->assertEqual("No Data.", $stdout[0]);
    }

    public function test30_PrintLineThatIsTooLong() {
        global $mlm;

        // Get the list of subscribers to print
        $subs = $mlm->getSubscribersByList($this->testlist);
        foreach($subs as $s) {
            // Make the name line wrap for the first subscriber in the list
            # $s->firstname = "AAAAAAAAAABBBBBBBBBBAAA"; # for 12 pt font
            $s->firstname = "AAAAAAAAAABBBBBBBBBBAAAAAAA"; # for 10 pt font
            $s->lastname = "";
            break;
        }

        // Print the labels
        $lp = new LabelPrinter();
        $lp->setSubscriberList($subs);
        $lp->printLabels();

        // Make sure everything went terribly wrong
        $this->assertFalse($lp->isOk());
    }

    public function test40_PrintWrongNumberOfLines() {
        global $mlm;
// TODO: find scenario to force this error
//        $this->assertFalse("NOT IMPLEMENTED");
    }

    public function test50_PrintAddressThatExceedsColumnHeight() {
        global $mlm;
// TODO: find scenario to force this error
//        $this->assertFalse("NOT IMPLEMENTED");
    }
}
