<?php

class ListTest extends MLMTestCase
{
    public function ListTest() {
        $this->init('list');
    }

    public function test10_CreateList() {
        global $mlm;

        $this->id = $mlm->addList(
            $this->data('topicid1'),
            $this->data('name1'),
            $this->data('unitid1')
        );

        $this->assertTrue(isset($this->id));
        $this->assertTrue(is_numeric($this->id));
        $this->assertTrue($this->id > 0);

        // Go ahead and create two more lists
        $mlm->addList(
            $this->data('topicid2'),
            $this->data('name2'),
            $this->data('unitid2')
        );
        $mlm->addList(
            $this->data('topicid3'),
            $this->data('name3'),
            $this->data('unitid3')
        );
    }

    public function test20_GetList() {
        global $mlm;
        // Get a fully linked reference to the entity
        $list = $mlm->getList($this->id);
        $this->assertEqual($this->data('name1'), $list->name);
        $this->assertEqual($this->data('unitid1'), $list->unitid);
        $this->assertEqual($this->data('topicid1'), $list->topicid);
    }

    /*
     *  Utility function - make sure all of the topic IDs
     *  are present in the mailing lists
     */
    private function assertTopicsExist($mailinglists, $topicids) {
        foreach ($mailinglists as $list) {
            $key = array_search($list->topicid, $topicids);
            if ($key !== False) {
                unset($topicids[$key]);
            }
        }
        if (count($topicids) > 0) {
            $this->fail("Missing topics: " . implode(", ", $topicids));
        }
    }

    /*
     *  Utility function - make sure none of the topic IDs
     *  are present in the mailing lists
     */
    private function assertTopicsDontExist($mailinglists, $topicids) {
        $badtopicids = array();
        foreach ($mailinglists as $list) {
            $key = array_search($list->topicid, $topicids);
            if ($key !== False) {
                $badtopicids[] = $topicids[$key];
            }
        }
        if (count($badtopicids) > 0) {
            $this->fail("Unexpected topics: " . implode(", ",$badtopicids));
        }
    }

    public function test25_GetLists() {
        // Assume 2 lists already exist (from test1)
        global $mlm;

        // Retrieve all lists
        $lists = $mlm->getLists();

        $topicsToMatch = array(
            $this->data('topicid1'),
            $this->data('topicid2'));

        // Make sure result set contains the 2 created lists
        $this->assertTopicsExist($lists, $topicsToMatch);
    }

    public function test26_GetListsByUnit() {
        // Assume 2 lists exist with disjoint units A and B
        global $mlm;

        // Retrieve all lists for unit A
        $lists = $mlm->getListsByUnit($this->data('unitid1'));

        $topicsToMatch = array($this->data('topicid1'));	// Unit A
        $topicsToNotMatch = array($this->data('topicid2'));	// Unit B

        $this->assertTopicsExist($lists, $topicsToMatch);
        $this->assertTopicsDontExist($lists, $topicsToNotMatch);
    }

    public function test27_GetListsByUnits() {
        // Assume 3 lists exist with disjoint units A B and C
        global $mlm;

        // Retrieve all lists for unit A and B
        $lists = $mlm->getLists();

        $topicsToMatch = array(
            $this->data('topicid1'),	// Unit A
            $this->data('topicid2')	// Unit B
        );

        $topicsToNotMatch = array($this->data('topicid3'));	// Unit C

        // Make sure result set contains list for unit A and B but not C
        $this->assertTopicsExist($lists, $topicsToMatch);
        $this->assertTopicsDontExist($lists, $topicsToNotMatch);
    }

    public function test30_UpdateList() {
        global $mlm;

        // Get a fully linked reference to the entity
        $list = $mlm->getList($this->id);

        $list->name=$this->data('name2');

        $mlm->merge($list);

        $list = $mlm->getList($this->id);
        $this->assertEqual($this->data('name2'), $list->name);
    }

    public function test40_DeleteList() {
        global $mlm;
        // Get a fully linked reference to the entity
        $list = $mlm->getList($this->id);

        // Delete the entity
        $mlm->delete($list);

        // Query the db and make sure it's gone
        $list = $mlm->getList($this->id);
        $this->assertEqual(NULL, $list);

        // Go ahead and delete the other two lists
        $list = $mlm->getListByTopic($this->data('topicid2'));
	if ($list != null) {
          $mlm->delete($list);
        }
        $list = $mlm->getListByTopic($this->data('topicid3'));
	if ($list != null) {
          $mlm->delete($list);
        }
    }
}
