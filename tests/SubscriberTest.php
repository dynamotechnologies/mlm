<?php

class SubscriberTest extends MLMTestCase
{
    var $id1 = 0; // Subscriber #1 will test required fields
    var $id2 = 0; // Subscriber #2 will test required and optional fields
    var $testLists = array();

    public function SubscriberTest() {
        $this->init('subscriber');
    }

    /*
     *  Populate prerequisite test data
     */
    public function setUp() {
        global $mlm;
        // Make some lists to test with
        // These are retreived later on simply by calling getLists()
        $id = $mlm->addList(
            MLMTestData::getData('list','topicid1'),
            MLMTestData::getData('list','name1'),
            MLMTestData::getData('list','unitid1'));
        array_push($this->testLists, $id);
        $id = $mlm->addList(
            MLMTestData::getData('list','topicid2'),
            MLMTestData::getData('list','name2'),
            MLMTestData::getData('list','unitid2'));
        array_push($this->testLists, $id);
    }

    /*
     *  Clean up test data
     */
    public function tearDown() {
        global $mlm;
        // Remove the test lists
        foreach ($this->testLists as $l) {
            $entity = $mlm->getList($l);
            $mlm->delete($entity);
        }
        $this->testLists = array();
    }

    public function test10_CreateSubscriber() {
        global $mlm;

        $this->id1 = $mlm->addSubscriber(
            $this->data('firstname1'),
            $this->data('lastname1'),
            $this->data('salutation1'),
            $this->data('address1_1'),
            $this->data('address2_1'),
            $this->data('city1'),
            $this->data('state1'),
            $this->data('zip1')
        );

        $this->assertTrue(isset($this->id1));
        $this->assertTrue(is_numeric($this->id1));
        $this->assertTrue($this->id1 > 0);

        $subscriber1 = $mlm->getSubscriber($this->id1);
        $subscriptions1 = $mlm->getSubscriptions($subscriber1);
        $this->assertEqual(0, sizeof($subscriptions1));

        $this->id2 = $mlm->addSubscriber(
            $this->data('firstname1'),
            $this->data('lastname1'),
            $this->data('salutation1'),
            $this->data('address1_1'),
            $this->data('address2_1'),
            $this->data('city1'),
            $this->data('state1'),
            $this->data('zip1'),
            $this->data('email1'),
            $this->data('phone1'),
            $this->data('country1'),
            $this->data('organization1'),
            $this->data('title1'),
            $this->data('notes1')
        );

        $this->assertTrue(isset($this->id2));
        $this->assertTrue(is_numeric($this->id2));
        $this->assertTrue($this->id2 > 0);
        $this->assertNotEqual($this->id1, $this->id2);

        $subscriber2 = $mlm->getSubscriber($this->id2);
        $subscriptions2 = $mlm->getSubscriptions($subscriber2);
        $this->assertEqual(0, sizeof($subscriptions2));

        // Adding a duplicate subscriber should just return the id of the existing one
        $id1b = $mlm->addSubscriber(
            $this->data('firstname1'),
            $this->data('lastname1'),
            $this->data('salutation1'),
            $this->data('address1_1'),
            $this->data('address2_1'),
            $this->data('city1'),
            $this->data('state1'),
            $this->data('zip1')
        );
        $this->assertEqual($this->id1, $id1b);
    }

    public function test20_GetSubscriber() {
        global $mlm;
        // Get a fully linked reference to the entity
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $this->assertEqual($this->data('firstname1'), $subscriber1->firstname);
        $this->assertEqual($this->data('lastname1'), $subscriber1->lastname);
        $this->assertEqual($this->data('salutation1'), $subscriber1->salutation);
        $this->assertEqual($this->data('address1_1'), $subscriber1->address1);
        $this->assertEqual($this->data('address2_1'), $subscriber1->address2);
        $this->assertEqual($this->data('city1'), $subscriber1->city);
        $this->assertEqual($this->data('state1'), $subscriber1->state);
        $this->assertEqual($this->data('zip1'), $subscriber1->zip);

        $subscriber2 = $mlm->getSubscriber($this->id2);
        $this->assertEqual($this->data('firstname1'), $subscriber2->firstname);
        $this->assertEqual($this->data('lastname1'), $subscriber2->lastname);
        $this->assertEqual($this->data('salutation1'), $subscriber2->salutation);
        $this->assertEqual($this->data('address1_1'), $subscriber2->address1);
        $this->assertEqual($this->data('address2_1'), $subscriber2->address2);
        $this->assertEqual($this->data('city1'), $subscriber2->city);
        $this->assertEqual($this->data('state1'), $subscriber2->state);
        $this->assertEqual($this->data('zip1'), $subscriber2->zip);
        $this->assertEqual($this->data('email1'), $subscriber2->email);
        $this->assertEqual($this->data('phone1'), $subscriber2->phone);
        $this->assertEqual($this->data('country1'), $subscriber2->country);
        $this->assertEqual($this->data('organization1'), $subscriber2->organization);
        $this->assertEqual($this->data('title1'), $subscriber2->title);
        $this->assertEqual($this->data('notes1'), $subscriber2->notes);
    }

    public function test30_LinkSubscriber() {
        global $mlm;
        $lists = $mlm->getLists();

        // Get a fully linked reference to the entity
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $subscriber2 = $mlm->getSubscriber($this->id2);

        // Add the subscriber to all available lists
        $i=0;
        foreach ($lists as $l) {
            $i++;
            $mlm->link($subscriber1, $l);
            // Make sure the right number of subsciptions come back
            $subscriptions1 = $mlm->getSubscriptions($subscriber1);
            $this->assertEqual($i, sizeof($subscriptions1));
        }

        // Add the subscriber to all available lists
        $i=0;
        foreach ($lists as $l) {
            $i++;
            $mlm->link($subscriber2, $l);
            // Make sure the right number of subsciptions come back
            $subscriptions2 = $mlm->getSubscriptions($subscriber2);
            $this->assertEqual($i, sizeof($subscriptions2));
        }
    }

    public function test40_UpdateSubscriber() {
        global $mlm;

        // Get a fully linked reference to the entity
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $subscriber2 = $mlm->getSubscriber($this->id2);

        // update subscriber #1
        $subscriber1->firstname=$this->data('firstname2');
        $subscriber1->lastname=$this->data('lastname2');
        $subscriber1->salutation=$this->data('salutation2');
        $subscriber1->address1=$this->data('address1_2');
        $subscriber1->address2=$this->data('address2_2');
        $subscriber1->city=$this->data('city2');
        $subscriber1->state=$this->data('state2');
        $subscriber1->zip=$this->data('zip2');
        $mlm->merge($subscriber1);

        // update subscriber #2
        $subscriber2->firstname=$this->data('firstname2');
        $subscriber2->lastname=$this->data('lastname2');
        $subscriber2->salutation=$this->data('salutation2');
        $subscriber2->address1=$this->data('address1_2');
        $subscriber2->address2=$this->data('address2_2');
        $subscriber2->city=$this->data('city2');
        $subscriber2->state=$this->data('state2');
        $subscriber2->zip=$this->data('zip2');
        $subscriber2->email=$this->data('email2');
        $subscriber2->phone=$this->data('phone2');
        $subscriber2->country=$this->data('country2');
        $subscriber2->organization=$this->data('organization2');
        $subscriber2->title=$this->data('title2');
        $subscriber2->notes=$this->data('notes2');
        $mlm->merge($subscriber2);

        // confirm that subscriber #1 was successfully updated
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $this->assertEqual($this->data('firstname2'), $subscriber1->firstname);
        $this->assertEqual($this->data('lastname2'), $subscriber1->lastname);
        $this->assertEqual($this->data('salutation2'), $subscriber1->salutation);
        $this->assertEqual($this->data('address1_2'), $subscriber1->address1);
        $this->assertEqual($this->data('address2_2'), $subscriber1->address2);
        $this->assertEqual($this->data('city2'), $subscriber1->city);
        $this->assertEqual($this->data('state2'), $subscriber1->state);
        $this->assertEqual($this->data('zip2'), $subscriber1->zip);

        // confirm that subscriber #2 was successfully updated
        $subscriber2 = $mlm->getSubscriber($this->id2);
        $this->assertEqual($this->data('firstname2'), $subscriber2->firstname);
        $this->assertEqual($this->data('lastname2'), $subscriber2->lastname);
        $this->assertEqual($this->data('salutation2'), $subscriber2->salutation);
        $this->assertEqual($this->data('address1_2'), $subscriber2->address1);
        $this->assertEqual($this->data('address2_2'), $subscriber2->address2);
        $this->assertEqual($this->data('city2'), $subscriber2->city);
        $this->assertEqual($this->data('state2'), $subscriber2->state);
        $this->assertEqual($this->data('zip2'), $subscriber2->zip);
        $this->assertEqual($this->data('email2'), $subscriber2->email);
        $this->assertEqual($this->data('phone2'), $subscriber2->phone);
        $this->assertEqual($this->data('country2'), $subscriber2->country);
        $this->assertEqual($this->data('organization2'), $subscriber2->organization);
        $this->assertEqual($this->data('title2'), $subscriber2->title);
        $this->assertEqual($this->data('notes2'), $subscriber2->notes);
    }

    public function test50_UnlinkSubscriber() {
        global $mlm;
        // Get a fully linked reference to the entity
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $subscriber2 = $mlm->getSubscriber($this->id2);

        // Remove subscriptions for subscriber #1
        $subscriptions1 = $mlm->getSubscriptions($subscriber1);
        $i = sizeof($subscriptions1);
        foreach ($subscriptions1 as $l) {
            $i--;
            $list = $mlm->getList($l);
            $mlm->unlink($subscriber1, $list);
            // Make sure the right number of subsciptions come back
            $subscriptions1 = $mlm->getSubscriptions($subscriber1);
            $this->assertEqual($i, sizeof($subscriptions1));
        }

        // Remove subscriptions for subscriber #2
        $subscriptions2 = $mlm->getSubscriptions($subscriber2);
        $i = sizeof($subscriptions2);
        foreach ($subscriptions2 as $l) {
            $i--;
            $list = $mlm->getList($l);
            $mlm->unlink($subscriber2, $list);
            // Make sure the right number of subsciptions come back
            $subscriptions2 = $mlm->getSubscriptions($subscriber2);
            $this->assertEqual($i, sizeof($subscriptions2));
        }

        // Make sure the subsciptions are gone for subscriber #1
        $subscriptions1 = $mlm->getSubscriptions($subscriber1);
        $this->assertEqual(0, sizeof($subscriptions1));

        // Make sure the subsciptions are gone for subscriber #2
        $subscriptions2 = $mlm->getSubscriptions($subscriber2);
        $this->assertEqual(0, sizeof($subscriptions2));
    }

    public function test60_DeleteSubscriber() {
        global $mlm;
        // Get a fully linked reference to the entity
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $subscriber2 = $mlm->getSubscriber($this->id2);

        // Delete the entity
        $mlm->delete($subscriber1);
        $mlm->delete($subscriber2);

        // Query the db and make sure subscriber #1 is gone
        $subscriber1 = $mlm->getSubscriber($this->id1);
        $this->assertEqual(NULL, $subscriber1);

        // Query the db and make sure subscriber #2 is gone
        $subscriber2 = $mlm->getSubscriber($this->id2);
        $this->assertEqual(NULL, $subscriber2);
    }

    public function test70_ValidateLegacyData() {
        global $mlm;

        // get legacy data in the same format we're expecting from the CSV's
        $legacydata = $this->getLegacyData();

        // make sure the data is okay
        $debugmsg = $mlm->validateFileData($legacydata);
        $this->assertTrue($debugmsg==="");
        if (strlen($debugmsg) > 0)
            echo "<p>ERROR:" . $debugmsg . "</p>";

        // Screwing with the data a little bit should make validation fail
        $baddata = $legacydata . ',,,';
        $debugmsg = $mlm->validateFileData($baddata);
        $this->assertFalse($debugmsg==="");
        $this->assertTrue(strlen($debugmsg)>0);
    }

    public function test80_ImportLegacyData() {
        global $mlm;
        $listid = $this->testLists[0];

        // get legacy data in the same format we're expecting from the CSV's
        $legacydata = $this->getLegacyData();
        $num_legacy_subs = substr_count($legacydata, "\n");

        // before importing make sure this list is empty
        $subscribers = $mlm->getSubscribersByList($listid);
        $this->assertEqual(0, sizeof($subscribers));

        // import the data
        $mlm->addSubscribersFromFileData($listid, $legacydata, false);

        // Confirm we got back the same number we put in
        $subscribers = $mlm->getSubscribersByList($listid);
        $this->assertEqual($num_legacy_subs, sizeof($subscribers));

        // Check the data itself to make sure all values come back ok
        $this->assertTrue($this->hasLegacySub1($subscribers));
        $this->assertTrue($this->hasLegacySub2($subscribers));

        // clean up
        $this->cleanList($listid);
    }

    public function test85_ImportLegacyDataSkipFirstRow() {
        global $mlm;
        $listid = $this->testLists[0];

        // get legacy data in the same format we're expecting from the CSV's
        $legacydata = $this->getLegacyData();
        $num_legacy_subs = substr_count($legacydata, "\n");

        // before importing make sure this list is empty
        $subscribers = $mlm->getSubscribersByList($listid);
        $this->assertEqual(0, sizeof($subscribers));

        // import the data, passing parameter $skipFirstRow=true to skip the first row of data
        $mlm->addSubscribersFromFileData($listid, $legacydata, true);

        // Confirm we got back the same number we put in (minus first row)
        $subscribers = $mlm->getSubscribersByList($listid);
        $this->assertEqual($num_legacy_subs-1, sizeof($subscribers));

        // Check the data itself to make sure all values come back ok
        $this->assertTrue($this->hasLegacySub1($subscribers));
        $this->assertTrue($this->hasLegacySub2($subscribers));

        // clean up
        $this->cleanList($listid);
    }

    public function test90_GetListAsCSV() {
        global $mlm;
        $listid = $this->testLists[0];

        // import subscribers so we have something to test with
        $legacydata = $this->getLegacyData();
        $mlm->addSubscribersFromFileData($listid, $legacydata, true);

        // get CSV representation
        $exportdata = $mlm->getListAsCSV($listid);

        // use existing validation method to validate format
        $debugmsg = $mlm->validateFileData($exportdata);
        $this->assertTrue($debugmsg==="");

        // get arrays containing each line as a separate element
        $legacylines = explode("\r\n", $legacydata);
        $exportlines = explode("\r\n", $exportdata);

        // confirm both have the same number of lines
        $this->assertEqual(sizeof($legacylines), sizeof($exportlines));
        
        // loop over lines
        for ($i=0; $i<sizeof($legacylines); $i++) {
            $legacyfields = explode(',', $legacylines[$i]);
            $exportfields = explode(',', $exportlines[$i]);

            // confirm both have the same number of fields in this line
            $this->assertEqual(sizeof($legacyfields), sizeof($exportfields));

            // loop over fields, skipping garbage fields
            for ($j=2; $j<sizeof($legacyfields); $j++) {
                if ($j==9)
                    continue; // skip phone number
                // all fields should match
                $this->assertEqual($legacyfields[$j], $exportfields[$j]);
            }
        }

        // clean up
        $this->cleanList($listid);
    }

    public function test100_CopySubscribers() {
        global $mlm;
        $listA = $this->testLists[0];
        $listB = $this->testLists[1];

        $subsA = $mlm->getSubscribersByList($listA);
        $subsB = $mlm->getSubscribersByList($listB);

        // before starting make sure this lists are empty
        $this->assertEqual(0, sizeof($subsA));
        $this->assertEqual(0, sizeof($subsB));

        // import subscribers to list A
        $legacydata = $this->getLegacyData();
        $mlm->addSubscribersFromFileData($listA, $legacydata, false);
        $subsA = $mlm->getSubscribersByList($listA);

        // Copy the subscribers from list A to list B
        $mlm->copySubscribers($listA, $listB);

        // get list of subscribers again to see if changes have taken effect
        $subsA2= $mlm->getSubscribersByList($listA);
        $subsB = $mlm->getSubscribersByList($listB);

        // confirm list A has not been tampered with
        $this->compareLists($subsA, $subsA2);

        // compare A and B. They should now be identical
        $this->compareLists($subsA, $subsB);

        // clean up
        $this->cleanList($listA);
        $this->cleanList($listB);
    }


    /*
     *  See constant CSV_FIELD_NAMES_LINE in the config file for current data file format.
     *  Unique ID, email, and Phone Number are considered a garbage fields and are not retained
     *  Country, Organization, Title, Notes are optional
     */
    private function getLegacyData() {
        return CSV_FIELD_NAMES_LINE . $this->getLegacyData1() . "\r\n" . $this->getLegacyData2() . "\r\n";
    }

    private function getLegacyData1() {
        $garbage = 'asd\'"fq+|werty@yahoo.com!%@%^$#^$&&**%()_';
        return $garbage . ',' .
            $garbage . ',' .
            $this->data('firstname1') . ',' .
            $this->data('lastname1') . ',' .
            $this->data('address1_1') . ',' .
            $this->data('address2_1') . ',' .
            $this->data('city1') . ',' .
            $this->data('state1') . ',' .
            $this->data('zip1') . ',' .
            $garbage . ',' .
            "" . ',' .
            "" . ',' .
            "" . ',' .
            "";
    }

    private function getLegacyData2() {
        $garbage = 'asd\'"fq+|werty@yahoo.com!%@%^$#^$&&**%()_';
        return $garbage . ',' .
            $garbage . ',' .
            $this->data('firstname2') . ',' .
            $this->data('lastname2') . ',' .
            $this->data('address1_2') . ',' .
            $this->data('address2_2') . ',' .
            $this->data('city2') . ',' .
            $this->data('state2') . ',' .
            $this->data('zip2') . ',' .
            $garbage . ',' .
            $this->data('country2') . ',' .
            $this->data('organization2') . ',' .
            $this->data('title2') . ',' .
            $this->data('notes2');
    }

    /*
     *  Check if a list of subscribers contains legacy subscriber #1
     */
    private function hasLegacySub1($subscribers) {
        foreach ($subscribers as $sub) {
            if ($sub->lastname==$this->data('lastname1') &&
                    $sub->firstname==$this->data('firstname1') &&
                    $sub->address1==$this->data('address1_1') &&
                    $sub->address2==$this->data('address2_1') &&
                    $sub->city==$this->data('city1') &&
                    $sub->state==$this->data('state1') &&
                    $sub->zip==$this->data('zip1') &&
                    $sub->country==NULL &&
                    $sub->organization==NULL &&
                    $sub->title==NULL &&
                    $sub->notes==NULL) {
                return true;
            }
        }
        return false;
    }

    /*
     *  Check if a list of subscribers contains legacy subscriber #2
     */
    private function hasLegacySub2($subscribers) {
        foreach ($subscribers as $sub) {
            if ($sub->lastname==$this->data('lastname2') &&
                    $sub->firstname==$this->data('firstname2') &&
                    $sub->address1==$this->data('address1_2') &&
                    $sub->address2==$this->data('address2_2') &&
                    $sub->city==$this->data('city2') &&
                    $sub->state==$this->data('state2') &&
                    $sub->zip==$this->data('zip2') &&
                    $sub->country==$this->data('country2') &&
                    $sub->organization==$this->data('organization2') &&
                    $sub->title==$this->data('title2') &&
                    $sub->notes==$this->data('notes2')) {
                return true;
            }
        }
        return false;
    }

    /*
     *  Remove all subscribers from a list
     */
    private function cleanList($listid) {
        global $mlm;
        $subscribers = $mlm->getSubscribersByList($listid);
        foreach ($subscribers as $s) {
            $mlm->unlink($s, $mlm->getList($listid));
            $mlm->delete($s);
        }
        $subscribers = $mlm->getSubscribersByList($listid);
        $this->assertEqual(0, sizeof($subscribers));
    }

    /*
     *  Takes two lists of subscribers and confirms that they match
     */
    private function compareLists($subsA, $subsB) {
        // bail if size mismatch to avoid potentially confusing undefined index exceptions
        $this->assertEqual(sizeof($subsA), sizeof($subsB));
        if (sizeof($subsA)!=sizeof($subsB)) {
            $this->fail("size mismatch. lists do not match. cannot continue");
            return;
        }

        // loop lists and compare ID's of subscribers
        for ($i=0; $i<sizeof($subsA); $i++) {
            // the following will fail if subscribers in both lists are not in same order
            $this->assertEqual($subsA[$i]->id, $subsB[$i]->id);
        }
    }
}
