<?php

class MLMTestCase extends UnitTestCase
{
    protected $id = 0;
    private $dataclass = "";

    protected function init($dc) {
        $this->dataclass = $dc;
        if (!MLMTestData::isValid($dc))
            throw new Exception("Initialization failed. Test data does not exist for class '$dc'.
                                 Append test data to the end of MLMTestCase.php.");
    }

    protected function data($key) {
        if (strlen($this->dataclass)<=0)
            return NULL;
        return MLMTestData::getData($this->dataclass, $key);
    }

    /*
     *  Everything that extends UnitTestCase must contain at least one 
     *  test or the stats will be funny
     */
    public function testMLMTestCaseExists() {
        $this->assertTrue(true);
    }

    public function log($mixed) {
        echo "<PRE>";
        var_dump($mixed);
        echo "</PRE>";
    }
}

class MLMTestData
{
    public static function getData($c, $k) {
        return MLMTestData::$testdata[$c][$k];
    }

    public static function isValid($c) {
        return array_key_exists($c, MLMTestData::$testdata);
    }

    private static $testdata = array(
        "subscriber" => array(
            "firstname1" => "Testy",
            "lastname1" => "Tester",
            "salutation1" => "Mr.",
            "address1_1" => "2001 E. Plumb Lane",
            "address2_1" => "Suite A3",
            "city1" => "Reno",
            "state1" => "NV",
            "zip1" => "89502",
            "email1" => "testy@mailinator.com",
            "phone1" => "301-555-1212",
            "country1" => "United States",
            "organization1" => "US Civil Corps of Web Testers",
            "title1" => "Lead Tester",
            "notes1" => "test test test",
            "firstname2" => "Testy2",
            "lastname2" => "Tester2",
            "salutation2" => "Mr.2",
            "address1_2" => "2001 E. Plumb Lane2",
            "address2_2" => "APT B622",
            "city2" => "Reno2",
            "state2" => "MD",
            "zip2" => "90210",
            "email2" => "hagfish@mailinator.com",
            "phone2" => "703-555-1234 x999",
            "country2" => "Canada",
            "organization2" => "US Civil Corps of Web Testers2",
            "title2" => "Associate Tester",
            "notes2" => "nothing notable about this note"
        ),
        "list" => array(
            "topicid1" => "NEPA_001234_S",
            "topicid2" => "123400_SOPA",
            "topicid3" => "NEPA_004321_S",
            "name1" => "Test List",
            "name2" => "Test Hashmap",
            "name3" => "Test Otherlist",
            "unitid1" => "110417",
            "unitid2" => "110419",
            "unitid3" => "111005"
        ),
        "labelprinter" => array(
        ),
        "libmlm" => array(
        )
    );
}

?>
