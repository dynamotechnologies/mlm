<?php

class LibMLMTest extends MLMTestCase
{
    /*
     *  Constructor
     */
    public function LibMLMTest() {
        // Read any static test data defined in MLMTestCase.php
        $this->init('libmlm');
    }

    /*
     *  Prepopulate test data
     */
    public function setUp() {
        global $mlm;

        // Init any persistent test data here
    }


    /*
     *  Clean up test data
     */
    public function tearDown() {
        global $mlm;

        // Destroy any persistent test data here
    }


    // ~~~~~~~~~~~~~~~~~~~~~~
    //   Tests
    // ~~~~~~~~~~~~~~~~~~~~~~

    public function test10_decodeFile() {
        global $mlm;

        // Test plain ASCII data
        $asciidata = "abcdefghijklmnopqrstuvwxyz
                      ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $data = InputUtil::decodeFile($asciidata);
        $this->assertEqual($data, $asciidata);

        // Test UTF8 data
        //	The original string, decoded from HTML
        $str = html_entity_decode("&Agrave;", ENT_COMPAT, 'UTF-8');

        //	Binary string containing capital-A with grave accent,
        //	encoded in UTF8 (Unix), ending with Unix LF
        $utf8data = pack("H", "c3800a");

        $data = InputUtil::decodeFile($utf8data);
        $this->assertEqual($data, $str);

        // Test CP-1252 data
        //	The original string, decoded from HTML
        $str = html_entity_decode("&Agrave;", ENT_COMPAT, 'UTF-8');

        //	Binary string containing capital-A with grave accent,
        //	encoded in CP1252 (DOS), ending with DOS CRLF
        $cp1252data = pack("H", "c00d0a");

        $data = InputUtil::decodeFile($cp1252data);
        $this->assertEqual($data, $str);
    }

    public function test20_implodeCSV() {
        global $mlm;

        // Test plain string data
        $fields = array("one", "two", "three");
        $implodedstr = $mlm->implodeCSV($fields);
        $this->assertEqual($implodedstr, "one,two,three");

        // Test string data with embedded commas or quotes
        $fields = array("one", "two,and-a", 'three"');
        $implodedstr = $mlm->implodeCSV($fields);
        $this->assertEqual($implodedstr, 'one,"two,and-a","three"""');

        // Test string data with newlines
        $fields = array("one", "two
", "three");
        $implodedstr = $mlm->implodeCSV($fields);
        $this->assertEqual($implodedstr, 'one,"two
",three');
    }

    public function test30_explodeCSV() {
        global $mlm;

        // Test plain string data
        $explodedstr = 'one, two, three';
        $ar = $mlm->explodeCSV($explodedstr);
        $this->assertEqual(count($ar), 3);

        // Test string data with embedded commas
        $explodedstr = 'one," two, and", three';
        $ar = $mlm->explodeCSV($explodedstr);
        $this->assertEqual(count($ar), 3);
        $this->assertEqual($ar[0], "one");
        $this->assertEqual($ar[1], " two, and");
        $this->assertEqual($ar[2], " three");

        // Test string data with newlines
        $explodedstr = 'one," two,
and", three';
        $ar = $mlm->explodeCSV($explodedstr);
        $this->assertEqual(count($ar), 3);
        $this->assertEqual($ar[0], "one");
        $this->assertEqual($ar[1], " two,
and");
        $this->assertEqual($ar[2], " three");
    }

    public function test40_isCompleteCSVRow() {
        global $mlm;

        // Test complete row
        $testcsvrow = 'one, two,"
        three"';
        $this->assertTrue($mlm->isCompleteCSVRow($testcsvrow));

        // Test complete row with quoted column
        $testcsvrow = 'one,"two"';
        $this->assertTrue($mlm->isCompleteCSVRow($testcsvrow));

        // Test complete row with leading space and unclosed quote
        //	(Because of the leading space, the loose quote is a literal)
        $testcsvrow = 'one, "two';
        $this->assertTrue($mlm->isCompleteCSVRow($testcsvrow));

        // Test incomplete row with unclosed quote
        $testcsvrow = 'one,"two';
        $this->assertFalse($mlm->isCompleteCSVRow($testcsvrow));

        // Test incomplete multiline row with unclosed quote
        $testcsvrow = 'one, two,"
        three';
        $this->assertFalse($mlm->isCompleteCSVRow($testcsvrow));
    }

    public function test50_modifyCSVRow() {
        global $mlm;

        // Addr1 not multiline
        $csvrow = 'id,email,first,last,addr1,addr2,city,state,zip,phone,country,org,title,notes';
        $row = $mlm->modifyCSVRow($csvrow);
        $this->assertEqual($row,
        'id,email,first,last,addr1,addr2,city,state,zip,phone,country,org,title,notes');

        // Addr1 multiline, addr2 empty
        $csvrow = 'id,email,first,last,"addr1
addr2",,city,state,zip,phone,country,org,title,notes';
        $row = $mlm->modifyCSVRow($csvrow);
        $this->assertEqual($row,
        'id,email,first,last,addr1,addr2,city,state,zip,phone,country,org,title,notes');

        // Addr1 multiline, addr2 nonempty
        $csvrow = 'id,email,first,last,"addr1
moreaddr1",addr2,city,state,zip,phone,country,org,title,notes';
        $row = $mlm->modifyCSVRow($csvrow);
        $this->assertEqual($row,
        'id,email,first,last,"addr1
moreaddr1",addr2,city,state,zip,phone,country,org,title,notes');
    }
}
