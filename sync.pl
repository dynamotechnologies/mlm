#!/usr/bin/perl
#
#  Script for nightly sync of GovDelivery topics
#
$now = localtime;
print "Started execution at $now\n";

$hostname = `hostname`;

use LWP::Simple;
use POSIX qw(strftime);
$SENDMAIL = "/usr/sbin/sendmail -t";

my $date = strftime('%Y/%m/%d', localtime);
my $datetime = strftime('%Y/%m/%d %H:%M:%S', localtime);
$SUBJECT = "Subject: MLM GovDelivery sync for $hostname $date";
$REPLY_TO = "Reply-to: fs-emnepa-support\@dynamotechnologies.com";
$SEND_TO = "To: fs-emnepa-support\@dynamotechnologies.com";

# Grab web page
$URL1 = 'http://localhost/mlm/sync.php';
$content1 = get $URL1;
$content1 = "Couldn't get $URL1" unless defined $content1;

$URL2 = 'http://localhost/mlm/sync-categories.php';
$content2 = get $URL2;
$content2 = "Couldn't get $URL2" unless defined $content2;

$now = localtime;
print "\nEnding execution at $now\n";

open (SENDMAIL, "|$SENDMAIL") or die "Cannot open $SENDMAIL $!";
print SENDMAIL "$REPLY_TO\n";
print SENDMAIL "$SUBJECT\n";
print SENDMAIL "$SEND_TO\n";
print SENDMAIL "Content-type: text/plain\n\n";
print SENDMAIL "$content1";
print SENDMAIL "$content2";
close (SENDMAIL);
