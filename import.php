<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');

    function fail($str) {
        echo "$str";
        exit(1);
    }

    $mlm = new MLM();

    if (InputUtil::isGetIntSafe('id')) {
        $id = $_GET['id'];
        $list = NULL;
        try {
            $list = $mlm->getList($id);
        } catch (Exception $e) {
            fail($e->getMessage());
        }
        if ($list == NULL) {
            fail("Could not find list with ID '$id'.");
        }
        $name = $list->name;
    } else if (InputUtil::isPostIntSafe('id') && InputUtil::postString('mlmaction')=='import') {
        $id = InputUtil::postString('id');
        $filedata = InputUtil::getFileData('importdata');

        $debugmsg = "";
        try {
            $debugmsg = $mlm->validateFileData($filedata);
        } catch (Exception $e) {
            fail($e->getMessage());
        }
        if ($debugmsg==="") {
            $mlm->addSubscribersFromFileData($id, $filedata, true);
        } else {
            fail("<p>Import failed.</p><br/><pre>" . $debugmsg . "</pre>");
        }

        header('Location: listhome.php?id=' . $id );	// redirect
        exit(0);
    } else {
        fail("Unknown request");
    }

    try {
        echo $mlm->getStdPageBegin('Import Legacy Data');
    } catch (Exception $e) {
        fail($e->getMessage());
    }

    echo "<h3>$name</h3>";
    echo '<a href="listhome.php?id=' . $id . '">&lt; Manage this list</a><br/>';
    echo '<br/>';
?>

<div class="form_line">
<p>Select a file to import:</p>
<form enctype="multipart/form-data" action="import.php" method="post">
<input name="id" type="hidden" value="<?php echo $id; ?>"/>
<input name="mlmaction" type="hidden" value="import"/>
<input name="importdata" type="file"/><br/>
<br/>
<input type="submit" id="submit" value="Import"/>
</form>
</div>
<br/>
<br/>

<?php
    try {
        echo $mlm->getStdPageEnd();
    } catch (Exception $e) {
        fail($e->getMessage());
    }
?>
