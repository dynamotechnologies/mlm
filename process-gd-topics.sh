#!/bin/bash

# Run hourly by root

cd /var/www/html/mlm/govdelivery/script

mailinglist='fs-emnepa-support@dynamotechnologies.com'

# Only mail output if >8 lines long (start and stop timestamps)

hostname=$(hostname)
output="$(./mlm-script 2>&1)"
linecnt=$(echo "$output" | wc -l)
if [ "$linecnt" -gt 8 ]; then
        echo "$output" | mail -s "GD output from $hostname" "$mailinglist"
fi
