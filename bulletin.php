<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');
    $mlm = new MLM();

    echo $mlm->getStdPageBegin('Create and Send E-mail');
    $mlmaction = InputUtil::postString('mlmaction');
    $remove = InputUtil::postString('Remove');

    if (InputUtil::isGetIntSafe('id')) {
        $id = $_GET['id'];
        $list = $mlm->getList($id);
        $name = $list->name;
        $code = $list->topicid;
        $bulletin_id = isset($_GET['bulletin_id']) ? $_GET['bulletin_id'] : null;
        $draft = $_GET['draft'];
        //$code = "FSPALS_40111";

			 	$attachment_name = "";
        if (strlen($bulletin_id)>0){
        	$bulletin = $mlm->getBulletinDetails($bulletin_id);
        	$subject = $bulletin->{"subject"};
        	$body = $bulletin->{"body"};
        	$attachment_name = $bulletin->{"bulletin-files"}->{"bulletin-file"}->{"name"};
        }
        else {
        	$subject='';
        	$body='<br /><br /><br /><hr/>Update your subscriptions, modify your password or email address, or stop subscriptions at any time on your <a href="https://public.govdelivery.com/accounts/USDAFS/subscriber/new?preferences=true">Subscriber Preferences Page</a>.'
        			.' You will need to use your email address to log in.'
        			. 'If you have questions or problems with the subscription service, please contact <a href="https://subscriberhelp.govdelivery.com">subscriberhelp.govdelivery.com</a>.'
					. '<br /><br />This service is provided to you at no charge by <a href="http://www.fs.fed.us/">US Forest Service</a>.';
        }

        echo "<h3>$name ($code)</h3>";
?>
        <script src="ckeditor/jquery.min.js"></script>
        <script src="ckeditor/ckeditor.js"></script>
        <script src="ckeditor/adapters/jquery.js"></script>

       	<script type="text/javascript">
            CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
        </script>

        <form enctype="multipart/form-data" action="bulletin.php" method="post">
        <input type="hidden" name="listtopicid" value="<?php echo $code; ?>" >
        <input type="hidden" name="bulletin_id" value="<?php echo $bulletin_id; ?>"/>
        <input name="id" type="hidden" value="<?php echo $id; ?>"/>
        <div class="container">
        <div class="form_line"><p><strong>Subject  </strong><input name="subject" type="text" size="84" value="<?php echo $subject; ?>"/>
        </p></div>
        <textarea class="ckeditor" name="body"><?php echo $body; ?></textarea>
        <br/>
<!--
COMMENTING OUT ATTACHMENT UNTIL FIXED IN GOVDELIVERY
        <div class="form_line"><p>
        <strong>Attachment: </strong>
        <?php
        	if (strlen($attachment_name)>0) {
        		echo $attachment_name;
        ?>
        <input style="margin-left:30px;" class="noborder" type="checkbox" id="remove" name="Remove" value="1"/>Remove
        <?php
        	}
		?>
        <input size="80" name="file" type="file" <?php if ($draft==0) {echo 'disabled';} ?>/>
 -->       <br/><br/>
        <input type="submit" name="mlmaction" id="submit" value="Send" <?php if ($draft==0) {echo 'disabled';} ?>/>
        <input type="submit" name="mlmaction" id="submit" value="Save as Draft" <?php if (strlen($bulletin_id)!=0 && $draft==0) {echo 'disabled';} ?>/>
<!--        <input type="submit" name="mlmaction" id="submit" value="Send Test E-mail" <?php if (strlen($bulletin_id)!=0 && $draft==0) {echo 'disabled';} ?>/>
        to <input name="email" type="text" size="42" value="<?php echo $_SESSION['user'];?>@fs.fed.us"/>
-->        </p>

	</div></div></form>
        <br/>
<?php
    } else if ($mlmaction=='Send' || $mlmaction=='Save as Draft' || $mlmaction=='Send Test E-mail') {
    	$bbody = $_POST['body'];
    	$bsubject = $_POST['subject'];
    	$btopic = $_POST['listtopicid'];
    	$bulletin_id = $_POST['bulletin_id'];
    	$id = $_POST['id'];

    	if ($mlmaction=='Send'){
			$result = $mlm->createBulletin($bulletin_id, $bbody, $bsubject, $btopic, $_FILES['file']['name'], $remove);
    		$message =  "The email was created and sent to " . $result->{"total-subscribers"} . " subscriber(s).";
    	}
    	else{
			if ($mlmaction=='Send Test E-mail'){
				$email = $_POST['email'];
			}

			$result = $mlm->manageDraft($bulletin_id, $bbody, $bsubject, $btopic, $_FILES['file']['name'],$remove,$email);

			if ($mlmaction=='Send Test E-mail'){
				$message = "The test email was sent to " . $email;
			}
			else {
				if (strlen($bulletin_id)==0) {
    				$message = "The draft was created.";
    			}
    			else {
					$message = "The draft was updated.";
				}
			}
    	}

    	echo '<script language="javascript">';
    	echo 'alert("' . $message . '");';
    	echo 'window.location.href = "listhome.php?id=' . $id . '";';
    	echo '</script>';

    } else {
        echo "<p>List not found.</p>";
        exit(1);
    }

	echo $mlm->getStdPageEnd();
?>
