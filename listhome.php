<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');

    $mlm = new MLM();
    if (!InputUtil::isGetIntSafe('id')) {
        echo "Could not find list with the specified ID.";
        exit(1);
    }
    $id = $_GET['id'];
    updateBreadcrumb($id);

    $list = $mlm->getList($id);
    if ($list == NULL) {
        echo "Could not find list with ID '$id'.";
        exit(1);
    }
    $name = $list->name;
    $projectid = MLM::getProjectIdFromTopicId($list->topicid);
    if (strlen($projectid)>0) {
        $name .= ' (' . $projectid . ')';
    }
    if ($list->mlmactive == false) {
        $name .= ' (DEACTIVATED)';
    }

    $subcount = sizeof($mlm->getSubscribersByList($id)) + sizeof($mlm->getCategorySubscribersByList($id));

    //$topic_id = 'NEPA_39451_S';
    $topic_id = $list->topicid;
    $bulletins = $mlm->getBulletins($topic_id);
    $drafts = $mlm->getDrafts($topic_id);

    echo $mlm->getStdPageBegin('Home');
    echo "<h3>" . htmlspecialchars($name) . "</h3>";

    echo '<div id="tab-container">';
    echo '<ul class="tab-menu">';
    echo '<li id="electronic" class="active">Electronic</li>';
    echo '<li id="postal">Postal</li>';
    echo '</ul>';

    echo '<div class="clear"></div>';
    echo '<div class="tab-top-border"></div>';
    echo '<div id="electronic-tab" class="tab-content active">';
    echo '<h3 class="h3g">Email Management</h3><br>';
    echo '<h3><div id="top_button"><a class="green_button" href="bulletin.php?id=' . $id . '&draft=1">Create Email</a></div></h3>';
    echo '<h3 class="h3o">DRAFTS</h3><br>';

    echo '<h4><TABLE width="800">';
    echo '<TR class="table-header"><TH width="12%">Date Modified</TH><TH>Subject</TH></TR>';
    foreach ($drafts as $draft) {
    	echo '<TR class="table-detail"><TD>' . date('Y-m-d', strtotime($draft->created)) . '</TD><TD><a href="bulletin.php?id=' . $id . '&bulletin_id=' . $draft->bulletin_id . '&draft=1">' . $draft->subject . '</a></TD></TR>';
    }
    echo '</TABLE></h4><br>';

    echo '<h3 class="h3o">SENT</h3><br>';
    echo '<h4><TABLE width="800">';
    echo '<TR class="table-header"><TH width="12%">Date Sent</TH><TH width="58%">Subject</TH><TH width="13%">Delivery Status</TH><TH width="17%">Subscribers Reached</TH></TR>';

    foreach ($bulletins as $bulletin) {
    	echo '<TR class="table-detail"><TD>' . date('Y-m-d', strtotime($bulletin->created)) . '</TD><TD><a href="bulletin.php?id=' . $id . '&bulletin_id=' . $bulletin->bulletin_id . '&draft=0">' . $bulletin->subject . '</a></TD><TD>Succeeded</TD><TD>' . $bulletin->recipient_count . '</TD></TR>';
    }

    echo '</TABLE></h4>';

    echo '<h3 class="h3o">SUBSCRIBERS</h3><br>';
    echo '<h4><TABLE width="800">';
    echo '<TR class="table-header"><TH>&nbsp;</TH></TR>';
    echo '<TR class="table-detail"><TD>';
    echo 'Do you need to upload electronic subscribers to this mailing list?<br><br>' .
				'Uploading electronic subscribers must be done through GovDelivery. GovDelivery users must have an account created for them by eMNEPA to access <a href="http://www.govdelivery.com"><font color="blue">www.govdelivery.com</font></a><br><br>' .
				'Project mailing list managers can either allow eMNEPA to upload subscribers or upload themselves.  Subscriber upload resources are available on the NSG Web site (<a href="http://fsteams.fs.fed.us/nfs_/nsg/emnepa/mlm/default.aspx"><font color="blue">http://fsteams.fs.fed.us/nfs_/nsg/emnepa/mlm/default.aspx</font></a>). ' .
    
'Please <a href="https://ems-team.usda.gov/sites/fs-emnepa-sd/SitePages/eMNEPA%20Support%20-%20My%20Requests.aspx" target="_top"><font color="blue">Click Here</font></a> for GovDelivery account creation and subscriber upload information.  <a href="https://admin.govdelivery.com/session/new" target="_new"><font color="blue">Admin login</font></a>';
    echo '</TD></TR></TABLE><h4>';



    echo '</div>';
    echo '<div id="postal-tab" class="tab-content">';

    $subscribers = $mlm->getSubscribersByList($id);
    $cat_subscribers = $mlm->getCategorySubscribersByList($id);

    echo '<ul class="nav">';
    if (sizeof($subscribers)>0) {
    echo '<li><a href="printlist.php?id=' . $id . '">Print Mailing Labels</a></li>';
    	echo '<li><a href="export.php?id=' . $id . '">Download CSV</a></li>';
        echo '<li><a href="copylist.php?id=' . $id . '">Copy Subscribers</a></li>';
    }
            		echo '<li><a href="editsubscriber.php?listid=' . $id . '">Add Subscriber</a></li>';
            		echo '<li><a href="import.php?id=' . $id . '">Import Legacy Data</a></li>';
            		echo '<li><a href="#subscribers">View Subscribers</a></li>';
    echo '<li><a href="#cat_subscribers">View Category Subscribers</a></li>';
            		echo '</ul>';

    echo '<a name="subscribers"></a>';
    echo "<h3>Subscribers</h3>";
            		if (sizeof($subscribers)>0) {
            		$subcount = sizeof($subscribers);
            		echo "<h5>Showing 1 to $subcount of $subcount subscribers to this list:</h5>";
            		echo '<table class="viewlist">';
            		echo "<tr>" .
            		"<th width=\"200\">Name/Organization</th>" .
            		"<th width=\"300\">Address</th>" .
            				"<th>Notes</th>" .
            				"<th>Action</th>" .
            						"</tr>";
        echo displaySubscribersTable($subscribers);
            echo "</table>";
            		} else {
            		echo "<h5>This list has zero subscribers.</h5>";
            		}

            			echo '<a name="cat_subscribers"></a>';
            					echo "<h3>Category Subscribers</h3>";
            							if (sizeof($cat_subscribers)>0) {
            					$subcount = sizeof($cat_subscribers);
            					echo "<h5>Showing 1 to $subcount of $subcount subscribers to categories associated with this list:</h5>";
            					echo '<table class="viewlist">';
        echo "<tr>" .
            					"<th width=\"200\">Name/Organization</th>" .
                "<th width=\"300\">Address</th>" .
                "<th>Notes</th>" .
                    "<th>Action</th>" .
            "</tr>";
                echo displaySubscribersTable($cat_subscribers);
                echo "</table>";
    } else {
            					echo "<h5>This list has zero category subscribers.</h5>";
            					}

    echo '</div>';
    echo '</div>';

    //echo '<p>Subscribers: ' . $subcount . '</p>';

    echo $mlm->getStdPageEnd();

    /*
     *  Maintain links to last five lists the user has viewed
    */
    function updateBreadcrumb($id) {
    	$recentlists = array();
    	if (isset($_SESSION['recentlists'])) {
    		$recentlists = $_SESSION['recentlists'];
    	}
    	if (in_array($id, $recentlists)) {
    		$key = array_search($id, $recentlists);
    		unset($recentlists[$key]);
    	} else {
    		if (sizeof($recentlists)>4) {
    			array_pop($recentlists);
    		}
    	}
    	array_unshift($recentlists, $id);
    	$_SESSION['recentlists'] = $recentlists;
    }

    function displaySubscribersTable($subscribers) {
    	$rval = "";
    	foreach ($subscribers as $s) {
    		$address = "";
    		$a1 = htmlspecialchars($s->address1);
    		$a2 = htmlspecialchars($s->address2);
    		if (strlen($a1)>0)
    			$address .= "$a1<br/>";
    		if (strlen($a2)>0)
    			$address .= "$a2<br/>";
    		$address .= htmlspecialchars("$s->city, $s->state $s->zip $s->country");
    		$email = trim($s->email);
    		$phone = trim($s->phone);
    		$notes = $s->notes;
    		if (strlen($notes)>100)
    			$notes = substr($notes, 0, 97) . "...";
    		$nameinfo = htmlspecialchars($s->lastname) . ", " . htmlspecialchars($s->firstname) . "<br/>" . htmlspecialchars($s->organization);
    		if (strlen($phone) > 0) {
    			$nameinfo .= "<br/>Phone: " . htmlspecialchars($phone);
    		}
    		if (strlen($email) > 0) {
    			$nameinfo .= "<br/>Email: " . htmlspecialchars($email);
    		}
    		$rval .= "<tr>" .
    				"<td>" . $nameinfo . "</td>" .
    				"<td>" . $address . "</td>" .
    				"<td>" . htmlspecialchars($notes) . "</td>" .
    				"<td><a href=\"editsubscriber.php?id=" . $s->id . "\">Edit</a><br/>" .
    				"<a href=\"viewsubscriber.php?id=" . $s->id . "\">View</a></td>" .
    				"</tr>";
    	}
    	return $rval;
    }

?>
