<?php

    require('lib/MLM.php');
    require('lib/InputUtil.php');
    $mlm = new MLM();

    if (InputUtil::isGetIntSafe('id')) {
        $csvdata = $mlm->getListAsCSV($_GET['id']);
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="subscribers.csv"');
        echo $csvdata;
    } else {
        echo "Could not find list with the specified ID.";
        exit(1);
    }

?>
