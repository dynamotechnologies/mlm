<?php

    require('lib/MLM.php');
    require('lib/InputUtil.php');
    require('lib/LabelPrinter.php');
    $mlm = new MLM();

    if (InputUtil::isGetIntSafe('id')) {
        $lp = new LabelPrinter();
        $listid = $_GET['id'];
        $subs = $mlm->getSubscribersByList($listid);
        $cat_subs = $mlm->getCategorySubscribersByList($listid);
        foreach ($cat_subs as $cs) {
            array_push($subs, $cs);
        }

        $lp->setSubscriberList($subs);
        $labelstatus = $lp->printLabels();
    }

    if ($labelstatus == 0) {
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="mailinglist.pdf"');
        readfile($lp->getPdfFileName());
    } else {
        echo "<p>Label printing failed.</p>";
        echo '<pre style="background-color:#999">' . $lp->getLogFileData() . "</pre>";
    }
?>
