<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');


    $mlm = new MLM();
    if (!InputUtil::isGetIntSafe('id')) {
        echo "Could not find list with the specified ID.";
        exit(1);
    }
    $id = $_GET['id'];
    //updateBreadcrumb($id);

    $list = $mlm->getList($id);
    if ($list == NULL) {
        echo "Could not find list with ID '$id'.";
        exit(1);
    }
    $name = $list->name;
    $projectid = MLM::getProjectIdFromTopicId($list->topicid);
    if (strlen($projectid)>0) {
        $name .= ' (' . $projectid . ')';
    }
    if ($list->mlmactive == false) {
        $name .= ' (DEACTIVATED)';
    }

    $subscribers = $mlm->getSubscribersByList($id);
    $cat_subscribers = $mlm->getCategorySubscribersByList($id);

    echo $mlm->getStdPageBegin('View List (Postal management options)');
    echo "<h3>" . htmlspecialchars($name) . "</h3>";

//    echo '<p><strong>Unit ID:</strong> ' . $list->unitid . '<br/>';
//    echo '<strong>Topic ID:</strong> ' . $list->topicid . '</p>';

    echo '<ul>';
    if (sizeof($subscribers)>0) {
        echo '<li><a href="printlist.php?id=' . $id . '">Print Mailing Labels</a></li>';
        echo '<li><a href="export.php?id=' . $id . '">Download CSV</a></li>';
        echo '<li><a href="copylist.php?id=' . $id . '">Copy Subscribers</a></li>';
    }
    echo '<li><a href="editsubscriber.php?listid=' . $id . '">Add Subscriber</a></li>';
    echo '<li><a href="import.php?id=' . $id . '">Import Legacy Data</a></li>';
    echo '<li><a href="#subscribers">View Subscribers</a></li>';
    echo '<li><a href="#cat_subscribers">View Category Subscribers</a></li>';
    echo '</ul>';

    echo '<a name="subscribers"></a>';
    echo "<h3>Subscribers</h3>";
    if (sizeof($subscribers)>0) {
        $subcount = sizeof($subscribers);
        echo "<h5>Showing 1 to $subcount of $subcount subscribers to this list:</h5>";
        echo '<table class="viewlist">';
        echo "<tr>" .
                "<th width=\"200\">Name/Organization</th>" .
                "<th width=\"300\">Address</th>" .
                "<th>Notes</th>" .
                "<th>Action</th>" .
            "</tr>";
        echo displaySubscribersTable($subscribers);
        echo "</table>";
    } else {
        echo "<h5>This list has zero subscribers.</h5>";
    }

    echo '<a name="cat_subscribers"></a>';
    echo "<h3>Category Subscribers</h3>";
    if (sizeof($cat_subscribers)>0) {
        $subcount = sizeof($cat_subscribers);
        echo "<h5>Showing 1 to $subcount of $subcount subscribers to categories associated with this list:</h5>";
        echo '<table class="viewlist">';
        echo "<tr>" .
                "<th width=\"200\">Name/Organization</th>" .
                "<th width=\"300\">Address</th>" .
                "<th>Notes</th>" .
                "<th>Action</th>" .
            "</tr>";
        echo displaySubscribersTable($cat_subscribers);
        echo "</table>";
    } else {
        echo "<h5>This list has zero category subscribers.</h5>";
    }


    echo $mlm->getStdPageEnd();


    /*
     *  Maintain links to last five lists the user has viewed
     
    function updateBreadcrumb($id) {
        $recentlists = array();
        if (isset($_SESSION['recentlists'])) {
            $recentlists = $_SESSION['recentlists'];
        }
        if (in_array($id, $recentlists)) {
            $key = array_search($id, $recentlists);
            unset($recentlists[$key]);
        } else {
            if (sizeof($recentlists)>4) {
                array_pop($recentlists);
            }
        }
        array_unshift($recentlists, $id);
        $_SESSION['recentlists'] = $recentlists;
    }
    
    */

    function displaySubscribersTable($subscribers) {
        $rval = "";
        foreach ($subscribers as $s) {
            $address = "";
            $a1 = htmlspecialchars($s->address1);
            $a2 = htmlspecialchars($s->address2);
            if (strlen($a1)>0)
                $address .= "$a1<br/>";
            if (strlen($a2)>0)
                $address .= "$a2<br/>";
            $address .= htmlspecialchars("$s->city, $s->state $s->zip $s->country");
            $email = trim($s->email);
            $phone = trim($s->phone);
            $notes = $s->notes;
            if (strlen($notes)>100)
                $notes = substr($notes, 0, 97) . "...";
            $nameinfo = htmlspecialchars($s->lastname) . ", " . htmlspecialchars($s->firstname) . "<br/>" . htmlspecialchars($s->organization);
            if (strlen($phone) > 0) {
                $nameinfo .= "<br/>Phone: " . htmlspecialchars($phone);
            }
            if (strlen($email) > 0) {
                $nameinfo .= "<br/>Email: " . htmlspecialchars($email);
            }
            $rval .= "<tr>" .
                    "<td>" . $nameinfo . "</td>" .
                    "<td>" . $address . "</td>" .
                    "<td>" . htmlspecialchars($notes) . "</td>" .
                    "<td><a href=\"editsubscriber.php?id=" . $s->id . "\">Edit</a><br/>" .
                    "<a href=\"viewsubscriber.php?id=" . $s->id . "\">View</a></td>" .
                "</tr>";
        }
        return $rval;
    }
?>
