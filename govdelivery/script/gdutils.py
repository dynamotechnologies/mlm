import sys, os
import xml.dom.minidom
from urllib2 import HTTPError
from xml.sax.saxutils import escape
import gdlogger
import ConfigParser

fname = os.path.join(os.path.dirname(__file__), "/var/www/common/dmUtil")
sys.path.insert(0, fname)
import dmUtil
dmUtil.initOpener()

myConf = ConfigParser.SafeConfigParser()
myConf.optionxform = str
myConf.read('/var/www/config/dmUtil-conf')

configs = {}
configs.update(myConf.items("GOVDELIVERY"))

GD_PREFIX = configs['PREFIX']
GD_ALLTOPICS = GD_PREFIX + "/topics.xml"
GD_SINGLETOPIC = GD_PREFIX + "/topics"
GD_ALLCATEGORIES = GD_PREFIX + "/categories.xml"
GD_SINGLECATEGORY = GD_PREFIX + "/categories"

GD_QS_DEFAULT = "CODE_RED"

def dry_run():
  gdlogger.log.info("Disabling gdutils")
  dmUtil.useTestMode()

#
#  Create a unique category as a lock record
#
def create_lockfile():
  return create_category("LOCKFILE", "LOCKFILE - DO NOT DELETE", "LOCKFILE", "Temporary system lock record, do not delete")

#
#  Remove lock record
#
def delete_lockfile():
  delete_category("LOCKFILE")

#
#  Generate printable exception message from HTTPException buffer
#
def get_httpexception_message(e):
  response_xml = e.read()
  dom = xml.dom.minidom.parseString(response_xml)
  gd_code = dom.getElementsByTagName("code")[0].firstChild.data
  gd_msg = dom.getElementsByTagName("error")[0].firstChild.data
  return gd_code + " " + gd_msg

#
#  Create topic
#
#  throws HTTPException
#
def create_topic(id, name, shortname, description):
  topic_xml = '								\
<?xml version="1.0" encoding="UTF-8"?>					\
<topic>									\
    <code>' + escape(id) + '</code>					\
    <default-pagewatch-results type="integer" nil="true"/>		\
    <description>' + escape(description) + '</description>		\
    <lock-version type="integer">0</lock-version>			\
    <name>' + escape(name) + '</name>					\
    <pagewatch-autosend type="boolean">false</pagewatch-autosend>	\
    <pagewatch-enabled type="boolean">false</pagewatch-enabled>		\
    <pagewatch-suspended type="boolean">false</pagewatch-suspended>	\
    <rss-feed-description nil="true"></rss-feed-description>		\
    <rss-feed-title></rss-feed-title>					\
    <rss-feed-url></rss-feed-url>					\
    <send-by-email-enabled type="boolean">false</send-by-email-enabled>	\
    <short-name>' + escape(shortname) + '</short-name>			\
    <subscribers-count type="integer">0</subscribers-count>		\
    <wireless-enabled type="boolean">false</wireless-enabled>		\
    <pages type="array"></pages>					\
</topic>'
  try:
    dmUtil.post(GD_ALLTOPICS, topic_xml.encode('utf-8'))
  except HTTPError, e:
    gdlogger.log.info(e.read())
    gdlogger.log.error('Could not create topic with ID: ' + id)
    gdlogger.log.error('  and name: ' + name)
    gdlogger.log.error('Please inspect GovDelivery for ID/name collisions!')
    gdlogger.log.error('==================================================')
    raise

def delete_topic(id):
  dmUtil.delete(GD_SINGLETOPIC + '/' + id + ".xml")

def get_topic(id):
  try:
    topic_xml = dmUtil.get(GD_SINGLETOPIC + '/' + id + ".xml").decode('utf-8')
    dom = xml.dom.minidom.parseString(topic_xml)
    topic = {}
    topic['id'] = id
    topic['name'] = dom.getElementsByTagName("name")[0].firstChild.data
    topic['shortname'] = dom.getElementsByTagName("name")[0].firstChild.data
    topic['description'] = dom.getElementsByTagName("name")[0].firstChild.data
    return topic
  except HTTPError, e:
    gdlogger.log.info(e.read())
    if (e.code == 404):
      return False
    else:
      raise

def get_all_topics_xml():
  try:
    return dmUtil.get(GD_ALLTOPICS).decode('utf-8')
  except HTTPError, e:
    gdlogger.log.info(e.read())
    raise

def get_all_topic_ids():
  topics_xml = get_all_topics_xml()
  topics_string = topics_xml.encode('ascii', 'replace')
  dom = xml.dom.minidom.parseString(topics_string)
  topics = dom.getElementsByTagName("topic")
  ids = [t.getElementsByTagName("code")[0].firstChild.data for t in topics]
  return ids

#
#  Create category
#
#  throws HTTPException
#
def create_category(id, name, shortname, description, parent = "", qs_page = GD_QS_DEFAULT):
  if (len(qs_page) > 0):
    qs_page = "<code>" + escape(qs_page) + "</code>"
  category_xml = '							\
<?xml version="1.0" encoding="UTF-8"?>					\
    <category>								\
    <allow-subscriptions type="boolean">true</allow-subscriptions>	\
    <code>' + escape(id) + '</code>					\
    <default-open type="boolean">true</default-open>			\
    <description>' + escape(description) + '</description>		\
    <lock-version type="integer">1</lock-version>			\
    <name>' + escape(name) + '</name>					\
    <short-name>' + escape(shortname) + '</short-name>			\
    <parent><code>' + escape(parent) + '</code></parent>		\
    <qs_page>' + qs_page + '</qs_page>				\
</category>'
  try:
    dmUtil.post(GD_ALLCATEGORIES, category_xml.encode('utf-8'))
  except HTTPError, e:
    gdlogger.log.info(e.read())
    raise

def delete_category(id):
  dmUtil.delete(GD_SINGLECATEGORY + '/' + id + ".xml")

def get_all_categories_xml():
  try:
    return dmUtil.get(GD_ALLCATEGORIES).decode('utf-8')
  except HTTPError, e:
    gdlogger.log.info(e.read())
    raise

def get_all_category_ids():
  categories_xml = get_all_categories_xml()
  dom = xml.dom.minidom.parseString(categories_xml)
  categories = dom.getElementsByTagName("category")
  ids = [c.getElementsByTagName("code")[0].firstChild.data for c in categories]
  return ids

def topic_get_category_ids(topicid):
  try:
    url = GD_SINGLETOPIC + '/' + topicid + "/categories.xml"
    categories_xml = dmUtil.get(GD_SINGLETOPIC + '/' + topicid + "/categories.xml").decode('utf-8')
    dom = xml.dom.minidom.parseString(categories_xml)
    idnodes = dom.getElementsByTagName("to-param")
    ids = [n.firstChild.data for n in idnodes]
    return ids
  except HTTPError, e:
    gdlogger.log.info(e.read())
    raise

def topic_assign_categories(topicid, catid_list):
  category_xml = '<topic><categories type="array">'
  for id in catid_list:
    category_xml += '<category><code>' + escape(id) + '</code></category>'
  category_xml += '</categories></topic>'
  try:
    dmUtil.put(GD_SINGLETOPIC + '/' + topicid + '/categories.xml', category_xml.encode('utf-8'))
  except HTTPError, e:
    gdlogger.log.info(e.read())
    raise

def topic_update_categories(topicid, add_catids = [], delete_catids = []):
  ids = topic_get_category_ids(topicid)
  set_ids = set(ids)
  set_add = set(add_catids)
  set_delete = set(delete_catids)
  newids = list((set_ids | set_add) - set_delete)
  topic_assign_categories(topicid, newids)

