import logging, logging.handlers

LOGNAME = "gdlog"
FILENAME = "log.txt"

log = logging.getLogger(LOGNAME)

def init():
	log.setLevel(logging.DEBUG)
	format = logging.Formatter("%(levelname)s %(asctime)s %(message)s")
	# logfile handler
	handler = logging.FileHandler(FILENAME)
	handler.setFormatter(format)
	handler.setLevel(logging.DEBUG)
	log.addHandler(handler)
	# console handler
	handler = logging.StreamHandler()
	handler.setFormatter(format)
	handler.setLevel(logging.DEBUG)
	log.addHandler(handler)

# Usage:
# import gdlogger
# gdlogger.init() # ONLY NEEDS TO BE DONE ONCE BY MAIN MODULE
#
# log = logging.getLogger("gdlog")
# OR
# log = logging.getLogger(logging.LOGNAME)
# OR
# log = gdlogger.log
#
# log.debug("debugmsg")
# log.info("infomsg")
# log.warn("warnmsg")
# log.error("errormsg")
# log.critical("critmsg")

