import sys, os
import xml.dom.minidom
from urllib2 import HTTPError
from xml.sax.saxutils import escape
import gdlogger
import ConfigParser

fname = os.path.join(os.path.dirname(__file__), "/var/www/common/dmUtil")
sys.path.insert(0, fname)
import dmUtil
dmUtil.initOpener()

myConf = ConfigParser.SafeConfigParser()
myConf.optionxform = str
myConf.read('/var/www/config/dmUtil-conf')

configs = {}
configs.update(myConf.items("DATAMART"))

DM_PREFIX = configs['PREFIX']
DM_PROJECTS = DM_PREFIX + "/projects"
DM_UNITS = DM_PREFIX + "/ref/units"
DM_PROJECT_PURPOSES = DM_PREFIX + "/ref/purposes"
DM_MLMPROJECTS = DM_PREFIX + "/mlm/projects"

def dry_run():
  gdlogger.log.info("Disabling dmutils")
  dmUtil.useTestMode()

def get_project_xml(projectid):
  return dmUtil.get(DM_PROJECTS + "/nepa/" + projectid).decode('utf-8')

def getElementText(dom, elementName):
  node = dom.getElementsByTagName(elementName)[0].firstChild
  if (node == None):
    return ''
  else:
    return node.data

#
#  Extract project members from xml
#
def decode_project_xml(project_xml):
  dom = xml.dom.minidom.parseString(project_xml)
  name = getElementText(dom, "name")
  description = getElementText(dom, "description")
  unitcode = getElementText(dom, "unitcode")
  purposeidnodes = dom.getElementsByTagName("purposeid")
  purposeids = [ p.firstChild.data for p in purposeidnodes ]
  project_data = {}
  project_data['name'] = name
  project_data['description'] = description
  project_data['unitcode'] = unitcode
  project_data['purposeids'] = purposeids
  return project_data

def get_unit_xml(unitid):
  return dmUtil.get(DM_UNITS + "/" + unitid).decode('utf-8')

def decode_unit_xml(unit_xml):
  dom = xml.dom.minidom.parseString(unit_xml)
  name = dom.getElementsByTagName("name")[0].firstChild.data
  return { 'name' : name }

def get_all_purpose_names():
  purposes = {}
  pp_xml = dmUtil.get(DM_PROJECT_PURPOSES).decode('utf-8')
  dom = xml.dom.minidom.parseString(pp_xml.encode('utf-8'))
  purposenodes = dom.getElementsByTagName("purpose")
  for pn in purposenodes:
    id = pn.getElementsByTagName("id")[0].firstChild.data
    name = pn.getElementsByTagName("name")[0].firstChild.data
    purposes[id] = name
  return purposes

def create_mlmproject(projectid, mlmid):
  mlm_xml = '<?xml version="1.0"?>'
  mlm_xml += '<mlmproject xmlns="http://www.fs.fed.us/nepa/schema">'
  mlm_xml += "<mlmid>" + escape(mlmid) + "</mlmid>"
  mlm_xml += "<projecttype>nepa</projecttype>"
  mlm_xml += "<projectid>" + escape(projectid) + "</projectid>"
  mlm_xml += "</mlmproject>"
  dmUtil.post(DM_MLMPROJECTS, mlm_xml.encode('utf-8'))

def get_mlmproject(mlmid):
  try:
    mlmproj_xml = dmUtil.get(DM_MLMPROJECTS + "/" + mlmid).decode('utf-8')
    dom = xml.dom.minidom.parseString(mlmproj_xml)
    projectid = dom.getElementsByTagName("projectid")[0].firstChild.data
    return projectid
  except HTTPError, e:
    if (e.code == 404):
      return False
    else:
      raise

def delete_mlmproject(mlmid):
  dmUtil.delete(DM_MLMPROJECTS + "/" + mlmid)

