<?php
    require('lib/MLM.php');
    $mlm = new MLM();

    echo $mlm->getStdPageBegin('Lists for user ' . $_SESSION['user']);

    echo "<h3>This project list is based on the forests to which you are linked.</h3>";

    // display header
    $lists = $mlm->getLists();
    
    if (sizeof($lists)>0) {
        echo '<table class="list1"><tr>' .
                '<td class="listleft"><h3 class="h3g">Mailing List</h3></td>' .
                //'<td class="listright"><strong>Subscriber Count</strong></td>' .
            '</tr></table>';
    }

    /*
     *  Lists are considered deactivated when GovDelivery no longer reports
     *  them in the list of active topics.
     *  We can continue to add subscribers to deactivated lists but this
     *  will seem strange to subscribers who may be expecting to get mail.
     */
    // loop over all available lists
    $toggler = false;
    $deactivated_lists = array();
    foreach ($lists as $l) {
        // filter deactivated lists from this table
        if ($l->mlmactive == false) {
          array_push($deactivated_lists, $l);
          continue;
        }
        // set alternating background color
        $liststyle = ($toggler) ? "list1" : "list0";

        $name = $l->name;
        $projectid = MLM::getProjectIdFromTopicId($l->topicid);
        if (strlen($projectid)>0) {
            $name .= ' (' . $projectid . ')';
        }
        echo '<table class="'.$liststyle.'"><tr>' .
                '<td class="listleft"><a href="listhome.php?id=' . $l->id  . '">' . $name . '</a></td>' .
                //'<td class="listright">' . $subcount . '</td>' .
            '</tr></table>';
        $toggler = !$toggler;
    }
    
    /* // Display table of deactivated mailing lists at bottom of page
    if (sizeof($deactivated_lists) > 0) {
        echo "<br/>";
        echo "<h2>Lists deactivated from GovDelivery:</h2>";
        echo "<br/>";
        echo '<table class="list1_inactive"><tr>' .
                '<td class="listleft"><strong>Mailing List</strong></td>' .
                //'<td class="listright"><strong>Subscriber Count</strong></td>' .
            '</tr></table>';
        $toggler = false;
        foreach ($deactivated_lists as $l) {
            $liststyle = ($toggler) ? "list1_inactive" : "list0_inactive";
            $subcount = sizeof($mlm->getSubscribersByList($l->id));

            $name = $l->name;
            $projectid = MLM::getProjectIdFromTopicId($l->topicid);
            if (strlen($projectid)>0) {
                $name .= ' (' . $projectid . ')';
            }
            echo '<table class="'.$liststyle.'"><tr>' .
                '<td class="listleft"><a href="listhome.php?id=' . $l->id  . '">' . $name . '</a></td>' .
                //'<td class="listright">' . $subcount . '</td>' .
                '</tr></table>';
            $toggler = !$toggler;
        }
    }
	
    */
    // if nothing showed up we best explain why
    if (sizeof($lists)==0) {
        echo "You do not have access to any mailing lists. Please contact the system administrator.";
    }

    echo $mlm->getStdPageEnd();
?>
