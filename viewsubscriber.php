<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');
    $mlm = new MLM();

    echo $mlm->getStdPageBegin('View Subscriber');

    $lists = $mlm->getLists();
    if (InputUtil::isGetIntSafe('id')) {
        $id = $_GET['id'];
        $sub = $mlm->getSubscriber($id);
        displaySubscriber($sub);
    } else {
        echo "<p>No subscriber found with the specified ID.</p>";
    }

    function displaySubscriber($sub) {
        global $mlm;

        echo '<ul>';
        echo '<li><a href="#subscriptions">View Subscriptions</a></li>';
        echo '<li><a href="#cat_subscriptions">View Category Subscriptions</a></li>';
        echo '</ul>';
        echo "<h3>Subscriber</h3>";

        echo MLM::getSubscriberLabel($sub);

        if (strlen($sub->organization)>0 || strlen($sub->title)>0 || strlen($sub->notes)>0) {
            echo "<h3>Extended Data</h3>";
            if (strlen($sub->email)>0)
                echo "Email: " . htmlspecialchars($sub->email) . "<br/>";
            if (strlen($sub->phone)>0)
                echo "Phone: " . htmlspecialchars($sub->phone) . "<br/>";
            if (strlen($sub->organization)>0)
                echo "Organization: " . htmlspecialchars($sub->organization) . "<br/>";
            if (strlen($sub->title)>0)
                echo "Title: " . htmlspecialchars($sub->title) . "<br/>";
            if (strlen($sub->notes)>0)
                echo "Notes: " . htmlspecialchars($sub->notes) . "<br/>";
        }

        $subscriptions = $mlm->getSubscriptions($sub);
        echo "<br/>";
        echo "<form action=\"editsubscriber.php\"  method=\"get\">" .
             "<input type=\"hidden\" name=\"id\" value=\"$sub->id\"/>" .
             "<input type=\"submit\" value=\"Edit\"/>" .
             "</form><br/>";
        echo "<form action=\"editsubscriber.php\" enctype=\"multipart/form-data\" method=\"post\">" .
             "<input type=\"hidden\" name=\"action\" value=\"delete\"/>" .
             "<input type=\"hidden\" name=\"id\" value=\"$sub->id\"/>" .
             "<input type=\"submit\" value=\"Delete\"/>" .
             "</form>";
        echo "<br/>";
        echo '<a name="subscriptions"></a>';
        echo "<h3>Subscriptions</h3>";
        foreach ($mlm->getLists() as $l) {
            $checked = (in_array($l->id, $subscriptions)) ? 'checked="yes"' : "";
            echo '<div><input type="checkbox" ' . $checked . ' disabled="disabled"' .
                'name="lists[]" value="' . $l->id . '"/>' . $l->name . '</div>';
        }
        echo "<br/>";

        echo '<a name="cat_subscriptions"></a>';
        echo "<h3>Category Subscriptions</h3>";
        echo $mlm->getCategoryTreeMarkup($sub->id, TRUE);
        echo "<br/>";

        echo "<a href=\"editsubscriber.php?id=$sub->id\">Edit</a>";
        echo "<form action=\"editsubscriber.php\" enctype=\"multipart/form-data\" method=\"post\">" .
             "<input type=\"hidden\" name=\"action\" value=\"delete\"/>" .
             "<input type=\"hidden\" name=\"id\" value=\"$sub->id\"/>" .
             "<input type=\"submit\" value=\"Delete\"/>" .
             "</form>";
        echo "<br/>";
    }

    echo $mlm->getStdPageEnd();
?>
