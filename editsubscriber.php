<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');
    $mlm = new MLM();

    function doRedirect($id) {
        $lastlist = MLM::getLastListId();
        if (is_numeric($lastlist) && $lastlist>0) {
            header("Location: listhome.php?id=$lastlist");
        } else {
            header("Location: viewsubscriber.php?id=$id");
        }
    }

    $action = "add"; // default: display interface to add a new subscriber
    $subscriptions = array();
    $lists = $mlm->getLists();
    $categories = $mlm->getCategories();
    $id = $salutation = $firstname = $lastname = $address1 = $address2 = $city = $state = $zip = $country = $organization = $title = $notes = $email = $phone = "";

    if (InputUtil::isGetIntSafe('id')) { // display interface to edit an existing subscriber
        $action = "update";
        $id = $_GET['id'];
        $sub = $mlm->getSubscriber($id);
        $salutation = $sub->salutation;
        $firstname = $sub->firstname;
        $lastname = $sub->lastname;
        $address1 = $sub->address1;
        $address2 = $sub->address2;
        $city = $sub->city;
        $state = $sub->state;
        $zip = $sub->zip;
        $email = $sub->email;
        $phone = $sub->phone;
        $country = $sub->country;
        $organization = $sub->organization;
        $title = $sub->title;
        $notes = $sub->notes;
        $subscriptions = $mlm->getSubscriptions($sub);
    } elseif (InputUtil::postString('action')=='add') { // processing POST data to add a new subscriber
        $id = $mlm->addSubscriber(InputUtil::postString('firstname'),
            InputUtil::postString('lastname'),
            InputUtil::postString('salutation'),
            InputUtil::postString('address1'),
            InputUtil::postString('address2'),
            InputUtil::postString('city'),
            InputUtil::postString('state'),
            InputUtil::postString('zip'),
            InputUtil::postString('email'),
            InputUtil::postString('phone'),
            InputUtil::postString('country'),
            InputUtil::postString('organization'),
            InputUtil::postString('title'),
            InputUtil::postString('notes'));
        $sub = $mlm->getSubscriber($id);
        $inputlists = InputUtil::postArray('lists');
        $inputcats = InputUtil::postArray('categories');
        foreach ($lists as $l) {
            foreach ($inputlists as $il) {
                if ($il == $l->id) {
                    $mlm->link($sub, $l);
                }
            }
        }
        foreach ($categories as $c) {
            foreach ($inputcats as $ic) {
                if ($ic == $c->code) {
                    $mlm->link($sub, $c);
                }
            }
        }
        doRedirect($id);
    } elseif (InputUtil::postString('action')=='update' && // processing POST data to update an existing subscriber
                InputUtil::isPostIntSafe('id')) {
        $id = $_POST['id'];
        $sub = $mlm->getSubscriber($id);
        $sub->firstname = InputUtil::postString('firstname');
        $sub->lastname = InputUtil::postString('lastname');
        $sub->salutation = InputUtil::postString('salutation');
        $sub->address1 = InputUtil::postString('address1');
        $sub->address2 = InputUtil::postString('address2');
        $sub->city = InputUtil::postString('city');
        $sub->state = InputUtil::postString('state');
        $sub->zip = InputUtil::postString('zip');
        $sub->email = InputUtil::postString('email');
        $sub->phone = InputUtil::postString('phone');
        $sub->country = InputUtil::postString('country');
        $sub->organization = InputUtil::postString('organization');
        $sub->title = InputUtil::postString('title');
        $sub->notes = InputUtil::postString('notes');
        $inputlists = InputUtil::postArray('lists');
        $inputcats = InputUtil::postArray('categories');
        $mlm->clearLinks($sub, 'list');
        $mlm->clearLinks($sub, 'category');
        foreach ($lists as $l) {
            foreach ($inputlists as $il) {
                if ($il == $l->id) {
                    $mlm->link($sub, $l);
                }
            }
        }
        foreach ($categories as $c) {
            foreach ($inputcats as $ic) {
                if ($ic == $c->code) {
                    $mlm->link($sub, $c);
                }
            }
        }
        $mlm->merge($sub);
        doRedirect($id);
    } elseif (InputUtil::postString('action')=='delete' && // processing POST data to delete an existing subscriber
                InputUtil::isPostIntSafe('id')) {
        $id = $_POST['id'];
        $sub = $mlm->getSubscriber($id);
        $mlm->delete($sub);
        $mlm->merge($sub);
        doRedirect($id);
    }

    $h = ($action=='update') ? 'Edit' : 'Add';
    echo $mlm->getStdPageBegin($h . ' Subscriber');

    if (InputUtil::isGetIntSafe('listid')) {
        echo '<br/><a href="listhome.php?id=' . $_GET['listid'] . '">&lt; Manage this list</a><br/>';
    }

    if ($action=='update') {
?>
<form action="editsubscriber.php" enctype="multipart/form-data" method="post">
<input type="hidden" name="action" value="delete"/>
<input type="hidden" name="id" value="<?php echo $sub->id; ?>"/>
<input type="submit" id="submit" value="Delete"/>
</form>
<?php
    } // endif

    echo '<ul class="nav">';
    echo '<li><a href="#subscriptions">Edit Subscriptions</a></li>';
    echo '<li><a href="#cat_subscriptions">Edit Category Subscriptions</a></li>';
    echo '</ul>';

?>
<form enctype="multipart/form-data" action="editsubscriber.php" method="post">
<input name="action" type="hidden" value="<?php echo $action; ?>"/>
<input name="id" type="hidden" value="<?php echo $id; ?>"/>
<h3>Subscriber</h3>
<table>
<div class="form_line"><p>
<tr><td>Salutation:</td><td><input name="salutation" type="text" size="15" value="<?php echo htmlspecialchars($salutation); ?>"/></td></tr>
<tr><td>First Name:</td><td><input name="firstname" type="text" size="45" value="<?php echo htmlspecialchars($firstname); ?>"/></td></tr>
<tr><td>Last Name:</td><td><input name="lastname" type="text" size="45" value="<?php echo htmlspecialchars($lastname); ?>"/></td></tr>
<tr><td>Address 1:</td><td><input name="address1" type="text" size="55" value="<?php echo htmlspecialchars($address1); ?>"/></td></tr>
<tr><td>Address 2:</td><td><input name="address2" type="text" size="55" value="<?php echo htmlspecialchars($address2); ?>"/></td></tr>
<tr><td>City, State Zip:</td><td><input name="city" type="text" value="<?php echo htmlspecialchars($city); ?>"/>,
<input name="state" type="text" size="2" value="<?php echo htmlspecialchars($state); ?>"/>
<input name="zip" type="text" size="10" value="<?php echo htmlspecialchars($zip); ?>"/></td></tr>
<tr><td>Country:</td><td><input name="country" type="text" size="55" value="<?php echo htmlspecialchars($country); ?>"/></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2"><h3>Extended Data:</h3></td></tr>
<tr><td>Email:</td><td><input name="email" type="text" size="55" value="<?php echo htmlspecialchars($email); ?>"/></td></tr>
<tr><td>Phone:</td><td><input name="phone" type="text" size="55" value="<?php echo htmlspecialchars($phone); ?>"/></td></tr>
<tr><td>Organization:</td><td><input name="organization" type="text" size="55" value="<?php echo htmlspecialchars($organization); ?>"/></td></tr>
<tr><td>Title:</td><td><input name="title" type="text" size="55" value="<?php echo htmlspecialchars($title); ?>"/></td></tr>
<tr><td>Notes:</td><td><textarea name="notes" cols="65" rows="5"><?php echo htmlspecialchars($notes); ?></textarea></td></tr>
</div></table>
<br/>
<input type="submit" id="submit" value="Save"/>
<br/>
<br/>

<a name="subscriptions"></a>
<h3>Subscriptions</h3>
<div class="form_line">
<?php
    foreach ($mlm->getLists() as $l) {
        $checked = "";
        $addNew = (InputUtil::isGetIntSafe('listid') && $l->id==$_GET['listid']);
        if ( in_array($l->id, $subscriptions) || $addNew) {
            $checked = 'checked="yes"';
        }

        $name = $l->name;
        $projectid = MLM::getProjectIdFromTopicId($l->topicid);
        if (strlen($projectid)>0) {
            $name .= ' (' . $projectid . ')';
        }

        echo '<div><p><input type="checkbox" ' . $checked .
            'name="lists[]" class="noborder" value="' . $l->id . '"/>' . $name . '</p></div>';
    }
?>

</div><br/>
<input type="submit" id="submit" value="Save"/>

<a name="cat_subscriptions"></a>
<h3>Category Subscriptions</h3>
<?php
    $subid = NULL;
    if (isset($sub))
        $subid = $sub->id;
    echo $mlm->getCategoryTreeMarkup($subid);
?>

<br/>
<input type="submit" id="submit" value="Save"/>
</form>

<?php
    echo $mlm->getStdPageEnd();
?>
