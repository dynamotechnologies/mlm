#!/bin/bash
#
#

cd /var/www/html/mlm-files/
DEPLOG=deployresults.txt

# Cron runs this file weekly on Thursday, but we only want to deploy on the
#   first thursday of each month. So if the day of the month is > 7 then 
#   just exit without taking any further action.
thisday=`date +"%d"`
if [ $thisday -gt 7 ]; then
    exit 0
fi

# Run deployment and record output
./mlm-deploy.sh --automode > $DEPLOG 2>&1

# mail results
cat $DEPLOG | mailx -s "MLM Deployment results for $HOSTNAME" fs-drm-alerts@phaseonecg.com
