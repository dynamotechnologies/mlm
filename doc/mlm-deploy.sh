#!/bin/bash
#
#

export JAVA_HOME=/usr/lib/jvm/jdk1.6.0_24
export PATH=$PATH:$JAVA_HOME/bin

WORKDIR=working
NOW=`date +"%Y.%m.%d-%H.%M.%S"`
TAGNAME=$HOSTNAME-release-$NOW
TAGDIR=$WORKDIR/tags/$TAGNAME
EXPORTDIR=$TAGNAME
TAGLOG=goodtags.log
LASTTAG=`tail -n 1 $TAGLOG`
PHP=/usr/local/php5/bin/php

if [ "$1" == "--automode" ] ; then
    AUTOMODE=1
fi

function testwork {
    $PHP $WORKDIR/trunk/TestRunner.php > testresults.tmp 2>&1
}

function testlive {
    $PHP $EXPORTDIR/TestRunner.php > testresults.tmp 2>&1
}

function rollback {
    ln -fsn mlm-files/$LASTTAG ../mlm
}

function testguts {
    if [ $AUTOMODE ]; then
        return 0
    fi
    echo "
You are about to push the latest trunk code into production.
Type 'yes' to proceed, if you've got the guts."
    read guts
    if [ "$guts" != "yes" ]; then
        echo "You may be a coward, but at least you're honest about it.
"
        exit 0
    fi
}

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     MLM Deployer v1.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Updating working copy and checking status..."
svn up $WORKDIR

# Check the output of `svn status`. Output should be empty.
STATUS=`svn status $WORKDIR`
if [ -n "$STATUS" ]; then
    echo "
Oh no, deployment failed! Working copy has modifications. Commit or roll back any local modifications before trying to deploy again. But really you shouldn't be making changes on this server anyway. Shame on you."
    exit 1
fi

# Run tests and save output to a file
echo "Running pre deploy test..."
testwork

# loop over test results and look for unexpected results
testlines=`cat testresults.tmp | wc -l`
echo ""
cat testresults.tmp

# If the tests failed, bail
if [ "$testlines" != "3" ]; then
    echo "
Tests failed. I won't deploy this crap. Fix these problems and try again."
    exit 1
fi

# ready to go. warn user this is last chance to cancel
testguts

# Create a tag
echo "
Creating a tag of this release for posterity..."
svn cp $WORKDIR/trunk $TAGDIR

# Exporting ensures that subversion .svn files are not exposed
echo "
Exporting a clean copy of the code..."
svn export $TAGDIR ./$EXPORTDIR

# Copy the config file manually, because it's not under source control
cp $WORKDIR/trunk/lib/MLM-CONF.php $EXPORTDIR/lib/

# Build the java classes
cd $EXPORTDIR/lib/LabelPrinter
./build
cd -

# Update symbolic link to complete the deployment
echo "
Linking exported files..."
ln -fsn mlm-files/$EXPORTDIR ../mlm
ls -al ../mlm

# Solution is now completely deployed. Run tests on the deployed directory 
#   just in case something weird happened.
echo "
Running post deploy test..."
testlive

# loop over test results and look for unexpected results
testlines=`cat testresults.tmp | wc -l`
echo ""
cat testresults.tmp

# If the tests failed, roll back to previous version
if [ "$testlines" != "3" ]; then
    echo "
OH NO!!! The tests failed! Rolling back all changes..."
    rollback
    echo "
The changes were rolled back. Inspect the test output for clues as to why this didn't work."
else
    # log the export dir so it can be used to roll back if next deploy fails
    echo $EXPORTDIR >> $TAGLOG

    # Commit the tag
    echo "
Committing tagged release..."
    svn ci $TAGDIR -m "Auto-tagging pilot release"

    echo "
SUCCESS!"
fi
