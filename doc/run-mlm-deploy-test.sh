#!/bin/bash
#
#
cd /var/www/html/mlm-files/
DEPLOG=deployresults.txt


# Run deployment and record output
./mlm-deploy.sh --automode > $DEPLOG 2>&1

# mail results
cat $DEPLOG | mailx -s "MLM Deployment results for $HOSTNAME" fs-drm-alerts@phaseonecg.com
