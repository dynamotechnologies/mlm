<?php

require('lib/MLM.php');
require('lib/LabelPrinter.php');
require('lib/InputUtil.php');
require('lib/simpletest/autorun.php');
require('tests/MLMTestCase.php');
$mlm = new MLM(TRUE);

//
// Run by viewing this file in a web browser or by command line like so:
//  php TestRunner.php
//
class TestRunner extends TestSuite {
    function TestRunner() {
        $this->TestSuite('All tests');

        $tests = array(
            "LibMLM",
            "Subscriber",
            "List",
            "LabelPrinter"
        );

        foreach ($tests as $t) {
            $this->addFile(dirname(__FILE__) . '/tests/' . $t . 'Test.php');
        }
    }
}
?>
