<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');


    $mlm = new MLM();
    if (!InputUtil::isGetIntSafe('id')) {
        echo "Could not find list with the specified ID.";
        exit(1);
    }
    $id = $_GET['id'];

    $list = $mlm->getList($id);
    if ($list == NULL) {
        echo "Could not find list with ID '$id'.";
        exit(1);
    }
    $name = $list->name;
    $projectid = MLM::getProjectIdFromTopicId($list->topicid);
    if (strlen($projectid)>0) {
        $name .= ' (' . $projectid . ')';
    }
    if ($list->mlmactive == false) {
        $name .= ' (DEACTIVATED)';
    }

    echo $mlm->getStdPageBegin('View List (Electronic management options)');
    echo "<h3>" . htmlspecialchars($name) . "</h3>";

    echo '<ul>';
    echo '<li><a href="bulletin.php?id=' . $id . '">Create/Send Bulletin</a></li>';
    echo '<li><a href="bulletin.php?id=' . $id . '" onclick="return false">Update Bulletin</a></li>';
    echo '<li><a href="bulletin.php?id=' . $id . '" onclick="return false">Create Draft Bulletin</a></li>';
    echo '</ul>';   

    echo $mlm->getStdPageEnd();
   
?>