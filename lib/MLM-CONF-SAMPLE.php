<?php

require_once("/var/www/common/parseini.php");

// if (!defined("ENABLE_TEST_LOGIN")) define("ENABLE_TEST_LOGIN", "1");

// NOTE: WORK_DIR must be updated in mlm-cleanup if it is changed here.
if (!defined("WORK_DIR")) define("WORK_DIR", "/tmp");

if (!defined("INSTALL_DIR")) define("INSTALL_DIR", dirname(__FILE__) . "/LabelPrinter");
if (!defined("JAVA_CLASSPATH")) define("JAVA_CLASSPATH", ".:".INSTALL_DIR.":/var/www/common/pdfbox/pdfbox.jar");

if (!defined("DB_CONN_STRING_TEST")) define("DB_CONN_STRING_TEST", "sqlite:/tmp/mlmtest.db");
if (!defined("DB_CONN_STRING")) define("DB_CONN_STRING", "mysql:host=localhost;dbname=mlm1");
if (!defined("DB_USER")) define("DB_USER", "mlm");
if (!defined("DB_PASS")) define("DB_PASS", "p1cgorcl");

if (!defined("ANONYMOUS_ROLE")) define("ANONYMOUS_ROLE", 8);	# The "anonymous" role ID

if (!defined("TOKEN_NAME")) define("TOKEN_NAME", 'token');

if (!defined("CSV_FIELDS_PER_LINE_MIN")) define("CSV_FIELDS_PER_LINE_MIN", 10);
if (!defined("CSV_FIELDS_PER_LINE_MAX")) define("CSV_FIELDS_PER_LINE_MAX", 14);
if (!defined("CSV_FIELD_NAMES_LINE")) define("CSV_FIELD_NAMES_LINE", "Unique ID,email,First Name,Last Name,Address1,Address2,City,State,Zip,Phone Number,Country,Organization,Title,Notes\r\n");

//the enepa user to run the cron scripts as.  Should be an administrator -- we should probably 
//rewrite the scripts so that they can skip permission checking and avoid the need for a user
//but that logic goes very deep.
if (!defined("ENEPA_SCRIPT_USER")) define("ENEPA_SCRIPT_USER", "samuelrcollins");

/* For TEST */
/*
if (!defined("GD_HOST")) define("GD_HOST", "stage-api.govdelivery.com");
if (!defined("GD_ACCT")) define("GD_ACCT", "FSPALS");

if (!defined("DM_PATH")) define("DM_PATH", "https://dmd-tst.ecosystem-management.org/api/1_0");
if (!defined("DM_USER")) define("DM_USER", "admin");
if (!defined("DM_PASS")) define("DM_PASS", "p1cgorcl");

*/

/* For PILOT */
/*
if (!defined("GD_HOST")) define("GD_HOST", "api.govdelivery.com");
if (!defined("GD_ACCT")) define("GD_ACCT", "USDAFS");

if (!defined("DM_PATH")) define("DM_PATH", "https://data.ecosystem-management.org/api/1_0");
if (!defined("DM_USER")) define("DM_USER", "admin");
if (!defined("DM_PASS")) define("DM_PASS", "D0ntP4niC");

*/

// Get GovDelivery credentials from config/dmUtil-conf
if (!defined("GD_USERPW")) define("GD_USERPW", get_govdelivery_userpwd());

?>
