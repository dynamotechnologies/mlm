/*
 * Read the address list from a file
 * Open the PDF template file (containing the label outlines)
 * Stamp out enough pages to contain the printed labels
 *     Print addresses to the top layer of each PDF page
 * Return 0 for no errors, 1 otherwise
 *
 * NOTES:
 *
 * PDF coordinates default to 72 units per inch
 * Origin is the bottom left corner
 *
 * The background of each page can be copied from the Avery 5150 label template,
 * but this is just for development QA, since the final PDF has no need for
 * the label outlines
 *
 * TODO:
 *
 * Allow configs
 * - input file name
 * - output file name
 * - log file name
 * - lines per address
 * - flag to use template file
 * - font size
 * - leading
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.util.Arrays;
import java.util.LinkedList;

import java.awt.geom.PathIterator;

import org.apache.pdfbox.exceptions.COSVisitorException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class LabelPrinter {

    public static String INFILE = "addresses.txt";
    public static String LOGFILE = "log.txt";
    public static String PDFFILE = "labelsout.pdf";
    // FIXME: This should probably be configurable
    public static String TEMPLATEPDF = "/var/www/html/mlm/lib/LabelPrinter/label-template.pdf";

    public final static int WARN_TEXT_WRAPPED = -1;
    public final static int ERR_CANT_WRAP = -2;
    public final static int ERR_TOO_TALL = -3;

    public static final int LABELS_PER_PAGE = 30;
    public static final int LABELS_PER_ROW = 3;
    public static final int LABELS_PER_COLUMN = 10;

    public static final int LINES_PER_ADDRESS = 6;	// from address file
    public static final int LINES_PER_LABEL = 5;	// to printed label

    public static final int LEADING_SIZE = 12;	// space between baselines
    public static final int FONT_SIZE = 10;	// font size in user units

    LinkedList<String> addresses = null;

    private static BufferedWriter logfile = null;
    private boolean exceptionsFound = false;

    public LabelPrinter() throws IOException {
        initLog();
    }

    /**
     * Creates a PDF document of mailing labels.
     * @param template the path to the PDF label template
     * @param fileout the path to the new PDF document
     * @throws    IOException 
     */
    public void createLabelsPdf(String template, String fileout) throws IOException, COSVisitorException {

        PDDocument reader = PDDocument.load(template);	// template
        PDDocument writer = new PDDocument();		// new PDF

        PDPage templatePage = (PDPage)reader.getDocumentCatalog().getAllPages().get(0);

        // Get # of addresses
        int numAddrs = addresses.size();

        // Compute # of pages
        int numPages = (int)((numAddrs - 1)/ LABELS_PER_PAGE) + 1;

        int addressIdx = 1;

        for (int i=0; i<numPages; i++) {

            // Copy template as new page
            PDPage page = (PDPage)writer.importPage(templatePage);

            // Add address text to each page content stream
            PDPageContentStream cstream = new PDPageContentStream(writer, page, true, false);

            for (int j=0; j<LABELS_PER_PAGE; j++) {

                // Add a label
                addAddress(cstream, addressIdx++);
                if (addressIdx > numAddrs) break;

            }
            cstream.close();
        }

        writer.save(fileout);
    }

    /*
     *	Print address to label page
     *	pcb is the page content layer where the label will be printed
     *	idx is the 1-based index of the address in the address list
     *
     *  Calculating column height
     *  =========================
     *  The "leading" (rhymes with "bedding") is the distance between
     *  baselines.
     *
     *  (All units are in "user units", defaulting to 72 DPI)
     *
     *  Therefore the expected non-wrapped vertical size of the text is
     *  leading * (number of lines)
     */
    void addAddress(PDPageContentStream pcs, int idx) throws IOException {

        pcs.saveGraphicsState();

        // Clip address text to a rect within the label area
        PDRectangle rect = getLabelRect(idx);
        pcs.addRect(rect.getLowerLeftX(), rect.getLowerLeftY(),
                     rect.getWidth(), rect.getHeight());
        pcs.clipPath(PathIterator.WIND_EVEN_ODD); // Wikipedia: Even-odd rule

        // fillRect(pcs, rect);	// DEBUG

        String address = getAddress(idx);
        int linesExpected = countLines(address);
        int expectedHeight = linesExpected * LEADING_SIZE;

        PDFont font = PDType1Font.HELVETICA;
        int rc = addTextToRect(pcs,
                      address,
                      rect,
                      font,
                      FONT_SIZE,
                      LEADING_SIZE);

        switch (rc) {
            case 0:
                // No errors
                break;
            case LabelPrinter.WARN_TEXT_WRAPPED:
                warn("line wrapped:\n" + address);
                break;
            case LabelPrinter.ERR_CANT_WRAP:
                error("Cannot wrap line, exceeds column width:\n" + address);
                drawRect(pcs, rect);
                drawCross(pcs, rect);
                break;
            case LabelPrinter.ERR_TOO_TALL:
                error("Address (including wrapped lines) exceeds column height:\n" + address);
                drawRect(pcs, rect);
                drawCross(pcs, rect);
                break;
            default:
                error("Unexpected error: " + rc + "\n" + address);
                drawRect(pcs, rect);
                drawCross(pcs, rect);
                break;
        }

        pcs.restoreGraphicsState();

    }

    /*
     *  Add message to content stream
     *  using the rectangle as a bounding box
     *
     *  Label text is permitted to word-wrap
     *
     *  Return 0 if successful
     *  Return WARN_TEXT_WRAPPED if any text wrapped
     *  Return ERR_CANT_WRAP if text could not wrap (word too long)
     *  Return ERR_TOO_TALL if text exceeded the given rectangle
     */
    int addTextToRect(PDPageContentStream pcs,
                      String msg,
                      PDRectangle rect,
                      PDFont font,
                      int fontsize,
                      int leading)
        throws IOException {

        int rc = 0;
        pcs.setFont(font, fontsize);

        int linenum = 0;
        LinkedList<String> lines = new LinkedList<String>(Arrays.asList(msg.split("\\n")));

        while (!lines.isEmpty()) {	// treat "lines" as a FIFO

            String line = lines.removeFirst();

            float x = rect.getLowerLeftX();
            float y = rect.getLowerLeftY() + rect.getHeight() - fontsize - (linenum * leading);

            if (rect.getLowerLeftY() > y) {
                rc = LabelPrinter.ERR_TOO_TALL;
                break;
            }

            float linewidth = font.getStringWidth(line) / 1000 * fontsize;

            if (linewidth > rect.getWidth()) {

                rc = LabelPrinter.WARN_TEXT_WRAPPED;

                // wrap text
                String[] splits = line.split("(?<=[\\s-])");

                int splitposn = 0;

                while (splitposn < splits.length) {
                    String testleft = "";
                    for (int i = 0; i <= splitposn; i++) {
                        testleft = testleft + splits[i];
                    }
                    float leftwidth = font.getStringWidth(testleft.trim()) / 1000 * fontsize;
                    if (leftwidth > rect.getWidth()) {
                        break;
                    }
                    splitposn++;
                }

                if (splitposn == 0) {
                    rc = LabelPrinter.ERR_CANT_WRAP;
                } else {
                    String leftpart = "";
                    String rightpart = "";
                    for (int i = 0; i < splitposn; i++) {
                        leftpart = leftpart + splits[i];
                    }
                    for (int i = splitposn; i < splits.length; i++) {
                        rightpart = rightpart + splits[i];
                    }
                    line = leftpart.trim();
                    lines.addFirst(rightpart.trim());
                }
            }
            pcs.beginText();
            pcs.moveTextPositionByAmount(x,y);
            pcs.drawString(line);
            pcs.endText();

            linenum++;
        }

        return rc;
    }

    /*
     *  Get addresses from a file
     *  Assume each address is LINES_PER_ADDRESS lines long
     *  Omit any blank lines
     */
    void loadAddresses(String filename) throws IOException {
        BufferedReader inputStream = null;

        addresses = new LinkedList<String>();

        try {
            inputStream = new BufferedReader(new FileReader(filename));

            String address = "";
            String l;
            int linecnt = 0;
            while ((l = inputStream.readLine()) != null) {
                if (l.length() != 0) {
                    address += l + "\n";
                }
                if (++linecnt == LINES_PER_ADDRESS) {
                    addresses.addLast(address);
                    linecnt = 0;
                    address = "";
                }
            }
            if (address.length() > 0) {
                    addresses.addLast(address);
                    warn("Address lines not a multiple of " + LINES_PER_ADDRESS + 
                        ". Possible wrong # of lines per address?");
            }
        } catch (FileNotFoundException e) {
            System.out.println("Exiting: Could not find address file " + filename);
            System.exit(1);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        info("Loaded " + addresses.size() + " addresses");
    }

    String getAddress(int idx) {
        if ((idx < 1) || (idx > addresses.size() + 1)) {
            return "";
        }
        return addresses.get(idx - 1);
    }

    /*
     *	For debugging - fill in the rect
     */
    void fillRect(PDPageContentStream ct, PDRectangle rect)
        throws IOException {
        // System.out.println("Drawing rect at (" + rect.getLowerLeftX() + "," + rect.getLowerLeftY() + "), (" + rect.getUpperRightX() + "," + rect.getUpperRightY() + ")");	// DEBUG
        ct.saveGraphicsState();
        ct.setNonStrokingColor(0xD0, 0xD0, 0xD0);
        ct.fillRect(rect.getLowerLeftX(), rect.getLowerLeftY(),
                     rect.getWidth(), rect.getHeight());
        ct.restoreGraphicsState();
    }

    /*
     *	For user errors - draw rect outline
     */
    void drawRect(PDPageContentStream ct, PDRectangle rect)
        throws IOException {
        ct.saveGraphicsState();
        ct.setStrokingColor(0x0, 0x0, 0x0);
        ct.addRect(rect.getLowerLeftX(), rect.getLowerLeftY(),
                     rect.getWidth(), rect.getHeight());
        ct.stroke();
        ct.restoreGraphicsState();
    }

    /*
     *	For user errors - cross out the rect
     */
    void drawCross(PDPageContentStream ct, PDRectangle rect)
        throws IOException {
        // System.out.println("Drawing rect at (" + rect.getLowerLeftX() + "," + rect.getLowerLeftY() + "), (" + rect.getUpperRightX() + "," + rect.getUpperRightY() + ")");	// DEBUG
        ct.saveGraphicsState();
        ct.setStrokingColor(0x0, 0x0, 0x0);
        float x1 = rect.getLowerLeftX();
        float x2 = x1 + rect.getWidth();
        float y1 = rect.getLowerLeftY();
        float y2 = y1 + rect.getHeight();
        ct.moveTo(x1, y1);
        ct.lineTo(x2, y2);
        ct.moveTo(x1, y2);
        ct.lineTo(x2, y1);
        ct.stroke();
        ct.restoreGraphicsState();
    }

    /*
     *	Generate rect coordinates for any label, based on the label index
     *  This was mostly trial and error
     *
     *  Labels are printed from left to right, top to bottom
     *
     *	labelidx is the 1-based index of the address in the address list
     */
    PDRectangle getLabelRect(int labelidx) {
      if (labelidx < 1) {
        return null;
      }
      // padding around the border of the page
      int border_x = (int)(4.2 / 16.0 * 72.0);
      int border_y = (int)(9.0 / 16.0 * 72.0);

      // padding between rows and columns
      int pad_x = 18;
      int pad_y = 8;

      // width and height of a single label
      int label_w = (int)(2.5 * 72);
      int label_h = 64;

      int pg_idx = (labelidx - 1) % LABELS_PER_PAGE;	// pg_idx = 0-29
      int row = (int)(pg_idx / LABELS_PER_ROW);		// row = 0-9
      row = LABELS_PER_COLUMN - row - 1;
      int col = pg_idx % LABELS_PER_ROW;		// col = 0-2

      int llx = border_x + (col * label_w) + (col * pad_x);
      int lly = border_y + (row * label_h) + (row * pad_y);

      int urx = llx + label_w;
      int ury = lly + label_h;

      PDRectangle rect = new PDRectangle();
      rect.setLowerLeftX(llx);
      rect.setLowerLeftY(lly);
      rect.setUpperRightX(urx);
      rect.setUpperRightY(ury);
      return rect;
    }

    /*
     *   Write info to log file
     */
    void info(String msg) {
      writeLogMsg("\n" + msg);
    }

    /*
     *   Write a warning to log file
     *   8/9/2012 - Suppress all warnings, clients don't want to see them
     */
    void warn(String msg) {
      // writeLogMsg("\n~~~~~~~~~~~~\n   WARNING:\n~~~~~~~~~~~~\n" + msg);
      // exceptionsFound = true;
    }

    /*
     *   Write an exception to the log file and flag an unrecoverable exception
     */
    void error(String msg) {
      writeLogMsg("\n~~~~~~~~~~~~\n    ERROR:\n~~~~~~~~~~~~\n" + msg);
      exceptionsFound = true;
    }

    void initLog() throws IOException {
        try {
            logfile = new BufferedWriter(new FileWriter(LOGFILE));
        } catch (FileNotFoundException e) {
            System.out.println("Exiting: Cannot write to log file " + LOGFILE);
            System.exit(1);
        }
    }

    /*
     * Note: Assumes input file is newline-terminated
     */
    int countLines(String s) {
      int cnt=0;
      for (int i=0; i<s.length(); i++) {
        if (s.charAt(i) == '\n') { cnt++; }
      }
      return cnt;
    }

    /*
     *   Do not use this method.
     *   Call info/warn/error instead
     */
    void writeLogMsg(String msg) {
        try {
            logfile.write(msg);
            logfile.newLine();
            logfile.flush();
        } catch (IOException e) {
            System.out.println("Fatal error writing log message, cannot continue");
            System.out.println("App error: " + msg);
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Main method
     * @param basename
     * @throws IOException
     */
    public static void main(String[] args) throws IOException, COSVisitorException {
        if (args.length > 0) {
            String basename = args[0];

            INFILE = basename + INFILE;
            LOGFILE = basename + LOGFILE;
            PDFFILE = basename + PDFFILE;
        }

        LabelPrinter lprint = new LabelPrinter();
        lprint.loadAddresses(INFILE);
        lprint.info("Starting run");
        lprint.info("------------");
        lprint.createLabelsPdf(TEMPLATEPDF, PDFFILE);
        lprint.info("Ending run");
        lprint.info("----------");
        if (lprint.exceptionsFound) {
            System.exit(1);
        }
    }
}
