import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

public class LabelDataGetter
{
    public String getData(String filein) throws IOException {
        PDDocument reader = PDDocument.load(filein);
        PDFTextStripper stripper = new PDFTextStripper();
        return stripper.getText(reader);
    }

    public static void main(String[] args) throws IOException {
        if (!(args.length>0)) {
            System.out.println("\nUsage:\nLabelDataGetter INFILE\nINFILE is the file you want me to read.");
            System.exit(1);
        }

        String pdffile = args[0];
        LabelDataGetter getter = new LabelDataGetter();
        System.out.println(getter.getData(pdffile));
    }
}
