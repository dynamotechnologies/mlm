<?php

class LabelPrinter
{
    var $basename = "";
    var $output = array();
    var $subscribers = array();
    var $status = NULL;

    public function LabelPrinter() {
    }

    /*
     *  Accepts an array of subscribers to be printed
     */
    public function setSubscriberList($subscribers) {
        $this->subscribers = $subscribers;
    }

    /*
     *  Prints data to a PDF file
     */
    public function printLabels() {
        $this->basename = WORK_DIR . "/" . uniqid("mlm-", true) . "-";
        if ($this->writeAddressFile()) {
            $cmd = "java -cp ".JAVA_CLASSPATH." LabelPrinter $this->basename 2>&1";
            exec($cmd, $this->output, $this->status);
        }
        return ($this->isOk()) ? 0 : 1;
    }

    /*
     *  Grabs text from the PDF file.
     *  For testing results.
     */
    public function getLabelData() {
        $thisdir = dirname(__FILE__);
        $cmd = "java -cp ".JAVA_CLASSPATH." LabelDataGetter " . $this->getPdfFileName() . " 2>&1";
        exec($cmd, $data, $rval);
        return $data;
    }

    public function getStdOut() {
        return $this->output;
    }

    public function getInFileName() {
        return $this->basename . "addresses.txt";
    }

    public function getPdfFileName() {
        return $this->basename . "labelsout.pdf";
    }

    public function getLogFileName() {
        return $this->basename . "log.txt";
    }

    public function getLogFileData() {
        $logfile = $this->getLogFileName();
        if (!is_readable($logfile))
            return "Log file not readable. $logfile";
        return file_get_contents($logfile);
    }

    public function isLogFileClean() {
        $logfile = $this->getLogFileName();
        if (!is_readable($logfile))
            return false;
        $logdata = file_get_contents($logfile);
        if (strpos("WARNING", $logdata)===FALSE && strpos("ERROR", $logdata)===FALSE) {
            return true;
        } else {
            return false;
        }
    }

    public function isPdfReadable() {
        $pdffile = $this->getPdfFileName();
        return is_readable($pdffile);
    }

    public function isOk() {

        if ( $this->isLogFileClean() &&
                $this->isPdfReadable() &&
                isset($this->status) &&
                $this->status===0 &&
                is_array($this->output) &&
                sizeof($this->output)==0 ) {
            return true;
        }
        return false;
    }


    // ~~~~~~~~~~~~~~~~~~~~~
    //   private
    // ~~~~~~~~~~~~~~~~~~~~~

    private function writeAddressFile() {
        $outstr = "";
        foreach ($this->subscribers as $s) {
            $outstr .= MLM::getSubscriberPrintLabel($s);
        }
        if (!strlen($outstr)>0) {
            $this->output[] = "No Data.";
            return false;
        }

        try {
            $afile = fopen($this->getInFileName(), "w");
            fwrite($afile, $outstr);
            fclose($afile);
            return true;
        } catch (Exception $e) {
            $this->output[] = "Error writing INFILE.";
        }
        return false;
    }
}
?>
