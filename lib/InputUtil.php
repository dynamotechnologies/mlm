<?php

class InputUtil
{

    public static function isGetIntSafe($i) {
        if (is_array($_GET) && array_key_exists($i, $_GET) && is_numeric($_GET[$i])) {
            return true;
        }
        return false;
    }

    public static function isPostIntSafe($i) {
        if (is_array($_POST) && array_key_exists($i, $_POST) && is_numeric($_POST[$i])) {
            return true;
        }
        return false;
    }

    // TODO: getter methods should start with 'get'
    public static function postString($s) {
        if (is_array($_POST) && array_key_exists($s, $_POST)) {
            return $_POST[$s];
        }
        return "";
    }

    // TODO: getter methods should start with 'get'
    public static function postArray($a) {
        if (is_array($_POST) && array_key_exists($a, $_POST) && is_array($_POST[$a])) {
            return $_POST[$a];
        }
        return array();
    }

    public static function isFileOk($name) {
        if ( array_key_exists($name, $_FILES) &&
                strlen($_FILES[$name]['tmp_name'])>0 &&
                is_readable($_FILES[$name]['tmp_name']) ) {
            return true;
        }
        return false;
    }

    /*
     *	Guess file encoding, return as UTF8
     */
    public static function decodeFile($content) {
        $input_encoding = "Windows-1252";
        if (@iconv('UTF-8', 'UTF-8//IGNORE', $content) == $content) {
            $input_encoding = "UTF-8";
        }

        return mb_convert_encoding($content, 'UTF-8', $input_encoding);
    }

    public static function getFileData($name) {
        if (InputUtil::isFileOk($name)) {
            $content = file_get_contents($_FILES[$name]['tmp_name']);
            return InputUtil::decodeFile($content);
        } else {
            return "";
        }
    }

}
?>
