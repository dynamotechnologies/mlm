<?php
error_reporting(0);
require('rb.php');
require('MLM-CONF.php');
require('GovDelivery.php');
require_once("DataMart.php");

/* CSV field definitions */
define("CSV_ID",		0);
define("CSV_EMAIL",		1);
define("CSV_FIRST_NAME",	2);
define("CSV_LAST_NAME",		3);
define("CSV_ADDRESS1",		4);
define("CSV_ADDRESS2",		5);
define("CSV_CITY",		6);
define("CSV_STATE",		7);
define("CSV_ZIP",		8);
define("CSV_PHONE",		9);
define("CSV_COUNTRY",		10);
define("CSV_ORGANIZATION",	11);
define("CSV_TITLE",		12);
define("CSV_NOTES",		13);

/*
    The primary purpose of this class is to abstract RedBean such that it is
    easy to swap out for some other database library if we want to.

    General purpose application methods can also be found here.
*/

class MLM
{
    private $testmode=FALSE;
    /*
     *  Constructor
     */
    public function MLM($testmode=FALSE) {
        if ($testmode===TRUE) {
            $this->testmode = TRUE;
            // Use Test database
            R::Setup( DB_CONN_STRING_TEST );

            // Log in as test user
            $loggedin = MLM::login('testytester', md5('testytesterMLMLOGIN'));
            $token = MLM::getToken();

            if ($loggedin && strlen($token)>0) {
                // Stash the token in $_GET so it can be read by checkSession()
                $_GET[TOKEN_NAME] = $token;
            } else {
                echo "Test Login Failed!";
                exit(1);
            }
        } else {
            // Use MLM database
            R::Setup( DB_CONN_STRING, DB_USER, DB_PASS );
#             R::Setup( DB_CONN_STRING );

            // Confirm that login was successful and redirect to error page if not logged in.
            MLM::checkSession();
        }
    }

    public function __destruct() {
        if ($this->testmode===TRUE) {
            MLM::logout();
        }
    }


    // --------------------------
    //   General Display Stuff
    // --------------------------


    /*
     *  Global Navigation
     */
    public function getStdPageBegin($title) {
        $user = (isset($_SESSION['user'])) ? $_SESSION['user'] : "";
        $rval = '<!doctype html><html><head>' .
                '<meta http-equiv="content-type" content="text/html; charset=UTF-8">' .
                '<title>Mailing List Manager - ' . $title . '</title>' .
                '<link href="media/css/mlm.css" rel="stylesheet" type="text/css" media="all"/>' .
                '<link rel="stylesheet" href="media/css/styles.css" type="text/css" media="screen" />' .
                '<link rel="stylesheet" type="text/css" href="media/PALS/pals_style.css">' .
                '<script type="text/javascript" src="jquery.js"></script>' .
                '<script type="text/javascript" src="scripts.js"></script>' .
            '</head><body>' .
            '<table class="main">' .
                '<tr><td colspan="2"><div id="headerbar">' .
                    '<a class="headernav" href="listindex.php">Home</a>' .
                    '<a class="headernav" href="logout.php">Log out</a> ' . $user .
                '</div></td></tr>' .
                '<tr><td class="leftnav"><div><ul class="nav">' .
                    '<li><a class="nav" href="listindex.php">MLM Home</a></li>';

        if (!isset($_SESSION['recentlists'])) {
            $_SESSION['recentlists'] = array();
        }
        $recentlists = $_SESSION['recentlists'];
        if (sizeof($recentlists)>0) {
            $rval .= '<br/><h2>Recent Lists:</h2>';
        }
        foreach ($recentlists as $id) {
            $list = $this->getList($id);
            if ($list==NULL) continue;
            $name = $list->name;
            if (strlen($name)>38) {
                $name = substr($name, 0, 35) . '...';
            }
            $rval .= '<li><a href="listhome.php?id=' . $id . '">' . $name . '</a></li>';
        }

        $rval .= '</ul></div></td><td>' .
            '<div id="maincol">' .
            '<h1>Mailing List Manager - ' . $title . '</h1>';
        return $rval;
    }

    /*
     *  Global Footer
     */
    public function getStdPageEnd() {
        return '</div></td></tr>' .
                '<tr><td colspan="2">
 				<div id="footer">' .
             		'&nbsp;' .
                '</div></td></tr></table></body></html>';
    }

    /*
     *  Label display should be consistent on every page
     */
    public static function getSubscriberLabel($sub) {
        $rval = "";
        if (strlen($sub->salutation)>0) {
            $rval .= htmlspecialchars($sub->salutation) . ' ';
        }
        $rval .= htmlspecialchars($sub->firstname) . ' ' . htmlspecialchars($sub->lastname) . '<br/>';
        if (strlen($sub->address1)>0) {
            $rval .= htmlspecialchars($sub->address1) . '<br/>';
        }
        if (strlen($sub->address2)>0) {
            $rval .= htmlspecialchars($sub->address2) . '<br/>';
        }
        if (($sub->country == "United States") ||
            ($sub->country == "")) {

            $rval .= htmlspecialchars($sub->city . ', ' . $sub->state . ' ' . $sub->zip) . '<br/>';
        } else {
						$rval .= htmlspecialchars($sub->city . ', ' . $sub->province . ' ' . $sub->zip) . '<br/>' . htmlspecialchars($sub->country) . '<br/>';
        }
        return $rval;
    }

    /*
     *  Generate label text for printing
     *  Expect up to 5 lines of text per label
     *
     *  The default US label format is:
     *    salutation firstname lastname
     *    organization
     *    address
     *    city state zip
     *
     *  The default international label format is:
     *    salutation firstname lastname
     *    organization
     *    address
     *    city province zip
     *    country
     */
    public static function getSubscriberPrintLabel($sub) {
        $rval = "";

        $salutation = trim($sub->salutation);
        $firstname = trim($sub->firstname);
        $lastname = trim($sub->lastname);
        $organization = trim($sub->organization);
        $address1 = trim($sub->address1);
        $address2 = trim($sub->address2);
        $city = trim($sub->city);
        $state = trim($sub->state);
        $zip = trim($sub->zip);
        $province = trim($sub->province);
        $country = trim($sub->country);

        if (($sub->country == "United States") ||
            ($sub->country == "")) {

            $line1 = trim("$salutation $firstname $lastname");
            if ((strlen($firstname) == 0) && (strlen($lastname) == 0)) {
                $line1 = "";
            }
            $line2 = "$organization";
            $line3 = "$address1";
            $line4 = "$address2";
            $line5 = "$city, $state $zip";
            $rval = "$line1\n$line2\n$line3\n$line4\n$line5\n\n";
        } else {
            $line1 = trim("$salutation $firstname $lastname");
            $line2 = "$organization";
            $line3 = "$address1";
            $line4 = "$address2";
            $line5 = "$city, $province $zip";
            $line6 = "$country";
            $rval = "$line1\n$line2\n$line3\n$line4\n$line5\n$line6\n";
        }
        return $rval;
    }


    function getCategoryTreeMarkup($subid, $readonly=FALSE) {
        $tree = '<ul class="tree"><li class="rootnode"><input type="checkbox" disabled="true"/>Root Category<ul>';
        foreach ($this->getCategories() as $c) {
            if ($c->parent_code == null) {
                $tree .= $this->getCategoryBranchMarkup($c, $subid, $readonly);
            }
        }
        $tree .= "</ul></li></ul>";
        return $tree;
    }


    function getCategoryBranchMarkup($category, $subid, $readonly) {
        if (!MLM::hasAccessToUnit($category->code))
            return "";
        $branch = "";
        $branch .= '<li>' . $this->getCategoryLeafMarkup($category, $subid, $readonly);
        $sub_branches = $this->getSubCategories($category);
        if (sizeof($sub_branches)>0) {
            $branch .= "<ul>";
            foreach ($sub_branches as $s) {
                $branch .= $this->getCategoryBranchMarkup($s, $subid, $readonly);
            }
            $branch .= "</ul>";
        }
        $branch .= "</li>";
        return $branch;
    }


    function getCategoryLeafMarkup($category, $subid, $readonly) {
        // Double-check permissions
        if (!MLM::hasAccessToUnit($category->code))
            return "";
        // Find out if the checkbox should be editable or not
        $disabled = "";
        if ($category->allow_subscriptions != "1" || $readonly) {
            $disabled = 'disabled="true"';
        }
        // Is the subscriber subscribed to this category?
        $checked = "";
        if ($this->isSubscribedToCategory($subid, $category->code))
            $checked = 'checked="true"';
        return '<input type="checkbox" ' . $disabled . $checked . ' name="categories[]" value="' . $category->code . '"/>' . $category->short_name . " (" . $category->code . ")";
    }


    /*
     *  Accepts a GovDelivery Topic ID and returns our best guess at the PALS project ID
     *  Returns an empty string if we can't figure it out.
     */
    public static function getProjectIdFromTopicId($topicid) {
        $topicarr = explode('_', $topicid);
        if ($topicarr != FALSE &&
                is_array($topicarr) &&
                sizeof($topicarr)==3 &&
                $topicarr[0]=='NEPA' &&
                $topicarr[2]=='S' &&
                is_numeric($topicarr[1])) {
            return $topicarr[1];
        }
        return "";
    }


    // --------------------------
    //   cr[UD]
    // --------------------------
    //   All of these Update/Delete methods are generic and
    //   can be used on any RedBean bean object
    //   TODO: update/delete methods are not limited by user privileges but probably should be.


    /*
     *  Add new row or update if already exists
     */
    public function merge($bean) {
        R::store($bean);
    }

    public function delete($bean) {
        R::trash($bean);
    }

    /*
     *  Add Foreign Key between two bean objects
     */
    public function link($b1, $b2) {
        R::associate($b1, $b2);
    }

    /*
     *  Drop Foreign Key between two bean objects
     */
    public function unlink($b1, $b2) {
        R::unassociate($b1, $b2);
    }

    /*
     *  Drop all Foreign Keys of a certain type (table)
     */
    public function clearLinks($b, $table) {
        return R::clearRelations( $b, $table );
    }

    public function beginTrans() {
        R::begin();
    }

    public function commitTrans() {
        R::commit();
    }

    public function rollbackTrans() {
        R::rollback();
    }

    // --------------------------
    //   Mailing Lists
    // --------------------------


    /*
     *  Returns ID after adding a new row to the database
     *  Returns existing ID if topicid is already in use
     *
     *  Not limited by user privileges. Should be called only by admin/script
     */
    public function addList($topicid, $name, $unitid) {
			$b = R::dispense( 'list' );
			$b->name = $name;
			$b->unitid = $unitid;
			$b->topicid = $topicid;
			$b->gdactive = true;
			$b->mlmactive = true;

			// topicid must be unique
			$l = R::findOne('list', 'topicid=?', array($topicid));
        if ($l == null) {
				return R::store($b);
        } else {
				return $l->id;	// already exists
			}
    }

    /*
     *  Get a list by its ID
     */
    public function getList($id) {
			$l = R::load('list', $id);
			if ($l->id==0 || !MLM::hasAccessToUnit($l->unitid)) {
					return NULL;
			}
			return $l;
    }


    /*
     *  Get a list by its GovDelivery Topic ID
     *  or return null
     */
    public function getListByTopic($topicid) {
        $l = R::findOne("list", "topicid=?", array($topicid));
        if ($l == null || $l->id==0 || !MLM::hasAccessToUnit($l->unitid)) {
            return NULL;
        }
        return $l;
    }

    /*
     *  Get the contents of a list in CSV format
     *  Export format should look like this:
     */
    public function getListAsCSV($id) {
        $subscribers = $this->getSubscribersByList($id);
        $rval = CSV_FIELD_NAMES_LINE;
        foreach ($subscribers as $sub) {
            $subarr = array($sub->id,
                                $sub->email,
                                $sub->firstname,
                                $sub->lastname,
                                $sub->address1,
                                $sub->address2,
                                $sub->city,
                                $sub->state,
                                $sub->zip,
                                $sub->phone,
                                $sub->country,
                                $sub->organization,
                                $sub->title,
                                $sub->notes);
            $rval .= $this->implodeCSV($subarr) . "\r\n";
        }
        return $rval;
    }


    /*
     *  Convert UnitRole unit IDs to normal form
     *  Remove 00 pairs from the end of the unit
     */
    public static function normalizeUnitID($unitid) {
        $normal = preg_replace("/(00)*$/", "", $unitid);
        return $normal;
    }

    /*
     *  Get an array of mailing lists limited by a single unit ID
     *  or return null
     */
    public function getListsByUnit($unitid) {
        if (!MLM::hasAccessToUnit($unitid)) {
            return NULL;
        }
        $normalizedunit = MLM::normalizeUnitID($unitid);
        $lists = R::find("list", "unitid like ?", array("${unitid}%"));
        usort($lists, 'sortLists');
        return $lists;
    }

    /*
     *  Get an array of mailing lists limited by a list of unit IDs
     *  or return null
     */
    public function getLists() {

    	$fp = fopen("/var/tmp/getLists.log", "w");
    	fwrite($fp, "Start getLists \n");
			$unitidarray = $_SESSION['units'];
			$unitCount = count($unitidarray);
        fwrite($fp, "# of units = $unitCount \n");
        if (!(count($unitidarray) > 0)) {
            // User has no unit roles, therefore no lists to review
            return array();
        }
        $clauses = array();
        $vars = array();
        foreach ($unitidarray as $unitid) {
            if (!MLM::hasAccessToUnit($unitid)) {
                continue;
            }
        fwrite($fp, "$unitid \n");
            $clauses[] = "unitid like ?";
            $normalizedunit = MLM::normalizeUnitID($unitid);
            $vars[] = "${normalizedunit}%";
        }
        $clausestr = implode(" or ", $clauses);
	$activeclause = ' AND mlmactive > 0';
	$totalclause = $clausestr . $activeclause;
        fwrite($fp, "About to call db. $totalclause \n");
        $lists = R::find("list", $totalclause, $vars);
        usort($lists, 'sortLists');
	//$limit='20';
	//$page=35;
	//$lists = array_slice($lists, (($page-1)*$limit), $limit, true);
        fwrite($fp, "$end \n");
        fclose($fp);
        return $lists;
    }

    /*
     *  Get an array of mailing lists limited by a list of unit IDs
     *  or return null
     */
    public function getLists1() {

    	$fp = fopen("/var/tmp/getLists.log", "w");
    	fwrite($fp, "Start getLists \n");
        $unitidarray = $_SESSION['units'];
        $unitCount = count($unitidarray);
        fwrite($fp, "# of units = $unitCount \n");
			if (!(count($unitidarray) > 0)) {
					// User has no unit roles, therefore no lists to review
					return array();
			}
			$clauses = array();
			$vars = array();
$index = 0;
        foreach ($unitidarray as $unitid) {
					if (!MLM::hasAccessToUnit($unitid)) {
							continue;
					}
if ($index > 5){
	continue;
}
$index = $index+1;

        fwrite($fp, "$unitid \n");
					$clauses[] = "unitid like ?";
					$normalizedunit = MLM::normalizeUnitID($unitid);
					$vars[] = "${normalizedunit}%";
			}
			$clausestr = implode(" or ", $clauses);
        fwrite($fp, "About to call db. $claustr \n");
        $lists = R::find("list", $clausestr, $vars);
        usort($lists, 'sortLists');
        fwrite($fp, "$end \n");
        fclose($fp);
        return $lists;
    }
    /*
     *  Get an array of mailing lists limited by a list of unit IDs
     *  or return null
     */
    public function getLists2() {

    	$fp = fopen("/var/tmp/getLists.log", "w");
    	fwrite($fp, "Start getLists \n");
        $unitidarray = $_SESSION['units'];
        $unitCount = count($unitidarray);
        fwrite($fp, "# of units = $unitCount \n");
        if (!(count($unitidarray) > 0)) {
            // User has no unit roles, therefore no lists to review
            return array();
        }
        $clauses = array();
        $vars = array();

$index = 0;
        foreach ($unitidarray as $unitid) {
            if (!MLM::hasAccessToUnit($unitid)) {
                continue;
            }

if ($index < 4){
	continue;
}
$index = $index+1;

        fwrite($fp, "$unitid \n");
            $clauses[] = "unitid like ?";
            $normalizedunit = MLM::normalizeUnitID($unitid);
            $vars[] = "${normalizedunit}%";
        }
        $clausestr = implode(" or ", $clauses);
        fwrite($fp, "About to call db. $claustr \n");
			$lists = R::find("list", $clausestr, $vars);
        usort($lists, 'sortLists');
        fwrite($fp, "$end \n");
        fclose($fp);
			return $lists;
    }

    /*
     *  Synchronize list of mailing lists from GovDelivery
     *  Not limited by user privileges. Should be called only by admin/script
     */
    public function syncLists() {
				# Get list of mailing lists from GD
        $gdlists = array();
        try {
          $gdlists = gd_get_topics();
        } catch (Exception $e) {
          echo "Failed to get topic lists from GovDelivery, exiting\n";
          echo $e->getMessage() . "\n";
          return;
        }
        echo "Got mailing lists\n";

        # Get list of mailing list beans from MLM
        try {
          $mlmbeans = $this->getLists();
        } catch (Exception $e) {
          echo "Failed to retrieve topic lists from local DB, exiting\n";
          echo $e->getMessage() . "\n";
          return;
        }
        echo "Got list of beans\n";

        # Convert list of beans to list of arrays with topicid as key
        $mlmlists = array();
        foreach ($mlmbeans as $mlmbean) {
          if (isset($mlmbean->topicid)) {
            $topic = array();

            $id = $mlmbean->id;
            $name = $mlmbean->name;
            $unitid = $mlmbean->unitid;
            $topicid = $mlmbean->topicid;
            $gdactive = $mlmbean->gdactive ? true : false;
            $mlmactive = $mlmbean->mlmactive ? true : false;

            $topic['id'] = $id;
            $topic['name'] = $name;
            $topic['unitid'] = $unitid;
            $topic['topicid'] = $topicid;
            $topic['gdactive'] = $gdactive;
            $topic['mlmactive'] = $mlmactive;

            $mlmlists[$topicid] = $topic;
          }
        }
        echo "Converted beans into arrays\n";

        # Assign unit IDs to the topics in the GovDelivery list
        # (units are not automatically part of the GovDelivery data set)
        foreach ($gdlists as $key=>$gdlist) {
          $topicid = $gdlist['topicid'];
          $unitid = '';
          if (array_key_exists($topicid, $mlmlists) && $mlmlists[$topicid]['unitid'] != null) {
            $unitid = $mlmlists[$topicid]['unitid'];
          } else {
            try {
              $unitid = gd_get_unitid($topicid);
            } catch (Exception $e) {
              echo $e->getMessage() . "\n";
              // No unitid available, fall through for error handling
            }
          }
          if (is_numeric($unitid)) {
            $gdlists[$topicid]['unitid'] = $unitid;
          } else {
            # If topic doesn't have a valid unit ID, discard it from list
            echo "No unit ID for topic ${topicid}, discarding\n";
            unset($gdlists[$key]);
          }
        }
        echo "Assigned unitids to GovDelivery mailing lists\n";

        # Get list of deleted MLM topic IDs, mark inactive lists
        $deleted_topics = array_diff_key($mlmlists, $gdlists);
        foreach ($deleted_topics as $topic) {
          $list = $this->getList($topic['id']);
          if ($list==NULL) continue;

          # Only mark/report lists as inactive if they're currently active
          if (($list->gdactive) || ($list->mlmactive)) {
              $list->gdactive = false;
              # If list is inactive in GovDelivery, go ahead and
              # deactivate it in MLM as well
              $list->mlmactive = false;
              $this->merge($list);
              echo "Deactivated list " . $topic['topicid'] . "\n";
          }
          # Only delete the MLMProject record if it currently exists
          if ($this->getMLMProjectFromDatamart($topic['topicid']) != NULL) {
              # Deactivate the MLMProject in the Datamart as well
              $this->deleteMLMProjectFromDatamart($topic['topicid']);
          }
        }
        echo "Marked inactive lists\n";

        # Get list of new GD topic IDs, add new lists
        $new_topics = array_diff_key($gdlists, $mlmlists);
        foreach ($new_topics as $topic) {
          $this->addList($topic['topicid'], $topic['name'], $topic['unitid']);
          echo "Added new list " . $topic['topicid'] . "\n";
        }
        echo "Added new lists\n";
    }

    /*
     *  Get the MLMProject record for the corresponding topicid
     */
    public function getMLMProjectFromDatamart($topicid) {
        $datamart = new DataMart(DM_USER, DM_PASS, DM_PATH);
        $mlmproject_xml = null;
        try {
            $mlmproject_xml = $datamart->getXml("/mlm/projects/$topicid");
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            return;
        }
        if ($mlmproject_xml == NULL) {
            return NULL;
        } else {
            return new SimpleXmlElement($mlmproject_xml);
        }
    }

    /*
     *  Delete the MLMProject record for the corresponding topicid
     *
     *  A topicid can look like NEPA_12345_S or USDAFS_123456789
     *  We are only interested in topics like NEPA_[numeric string]_S
     *  The numeric portion is the project ID
     *  The MLM record can be deleted from /mlm/projects/NEPA_12345_S
     */
    public function deleteMLMProjectFromDatamart($topicid) {
        if (preg_match("/^NEPA_[0-9]+_S$/", $topicid)) {
            $datamart = new DataMart(DM_USER, DM_PASS, DM_PATH);
            try {
                $datamart->delete("/mlm/projects/$topicid");
                echo "Deactivated MLMProject $topicid\n";
            } catch (Exception $e) {
                echo $e->getMessage() . "\n";
                return;
            }
        }
    }


    // --------------------------
    //   Subscribers
    // --------------------------

		/*
     *  Get new snailmail subscribers from DataMart (MailingListSubscribers)
     *
     *  Retrieves list of unprocessed subscribers using the service at
     *  /utils/subscribers/unsubscribed?activetopic=1&contactmethod=Mail
     *
     *  Marks processed subscribers as complete by setting "subscribeddate"
     *  in the Datamart record
     */
    public function addSubscribersFromDatamart() {

			echo "Processing new postal mail subscribers\n";

        # get list of new subscribers
 			$datamart = new DataMart(DM_USER, DM_PASS, DM_PATH);
        $subscribers_xml = null;
        try {
            # only look at pending subscriptions for active mailing lists
            $subscribers_xml = $datamart->getXml("/utils/subscribers/unsubscribed?activetopic=1&contactmethod=Mail");
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            return;
        }
        if ($subscribers_xml == NULL) {
            echo "Error getting list of new subscribers\n";
            echo "HTTP rc = " . $datamart->getHttpRC();
            return;
        }
        $subscribers = new SimpleXmlElement($subscribers_xml);

        foreach ($subscribers as $sub) {
              # only look at subscribers with contactmethod of "Mail"
              if (strval($sub->contactmethod) != "Mail") {
						continue;
					}
              $subscriberid = strval($sub->subscriberid);
              $name = trim(strval($sub->lastname)) . ", " . trim(strval($sub->firstname));
              echo "Adding subscriber $name, id = $subscriberid\n"; // DEBUG

              # make sure active topic exists
              $caraprojid = strval($sub->caraprojectid);
              echo "Adding to CARA project $caraprojid\n";	// DEBUG
              $caraproj_xml = null;
              try {
                  $caraproj_xml = $datamart->getXml("/caraprojects/$caraprojid");
              } catch (Exception $e) {
                  echo $e->getMessage() . "\n";
                  return;
              }
              if ($caraproj_xml == null) {
                  echo "Error getting CARA project rec for ID $caraprojid\n";
                  echo "HTTP rc = " . $datamart->getHttpRC();
                  return;
              }
              $caraproj = new SimpleXmlElement($caraproj_xml);
              $palsid = strval($caraproj->palsid);
              echo "--> PALS project $palsid\n";	// DEBUG
              $topicid = "NEPA_${palsid}_S";
              echo "--> GD topic $topicid\n";		// DEBUG
              $list = $this->getListByTopic($topicid);
              if ($list == null) {
                  echo "Error getting mailing list for topic ID $topicid\n";
         		continue;
					}

              # validate subscriber data
              # require FirstName + LastName not null
              $lastname = trim(strval($sub->lastname));
              $firstname = trim(strval($sub->firstname));
              if (strlen($firstname . $lastname) == 0) {
                  echo "Skipping user with no name\n";
                  continue;
              }
              $title = trim(strval($sub->title));
              $org = trim(strval($sub->org));
              $orgtype = trim(strval($sub->orgtype));
              $subscribertype = trim(strval($sub->subscribertype));
              # require AddressStreet1 or AddressStreet2
              $addr1 = trim(strval($sub->addstreet1));
              $addr2 = trim(strval($sub->addstreet2));
              if (strlen($addr1 . $addr2) == 0) {
	        	echo "Skipping user $name with no street address\n";
         		continue;
					}
              # require City Zip
              $city = trim(strval($sub->city));
              if (strlen($city) == 0) {
						echo "Skipping user $name with no city\n";
						continue;
					}
              $zip = trim(strval($sub->zip));
              if (strlen($zip) == 0) {
					 	echo "Skipping user $name with no zip\n";
						continue;
					}
              # if Country = United States or null, require State
              # else require Province
              # (not really sure why CARA has both)
              $state = trim(strval($sub->state));
              $province = trim(strval($sub->province));
              $country = trim(strval($sub->country));
              if (($country == "United States") ||
                   strlen($country) == 0) {
                  $country = ""; // normalize USA to empty string
                  if (strlen($state) == 0) {
							echo "Skipping US user $name with no state\n";
							continue;
						}
              } else {
                  if (strlen($province) == 0) {
							echo "Skipping non-US user $name with no province\n";
							continue;
						}
                  $state = $province;
					}
              $email = trim(strval($sub->email));
              $phone = trim(strval($sub->phone));

              # create subscriber rec and link to mailing list
              $id = $this->addSubscriber($firstname,
                                         $lastname,
                                         $title,
                                         $addr1,
                                         $addr2,
                                         $city,
                                         $state,
                                         $zip,
                                         $email,
                                         $phone,
                                         $country);
					$subscriber = $this->getSubscriber($id);
					$this->link($subscriber, $list);

              # update datamart record with subscribeddate
              $timestamp = strftime("%Y-%m-%dT%H:%M:%S");
              $sub->addChild('subscribeddate', $timestamp);
              $sub['xmlns'] = 'http://www.fs.fed.us/nepa/schema';
              $subscriberURL = "/caraprojects/$caraprojid/mailinglist/subscribers/$subscriberid";
              echo "--> Subscriber URL = $subscriberURL\n";
              try {
                  $datamart->putXml($subscriberURL, strval($sub->asXML()));
              } catch (Exception $e) {
                  echo $e->getMessage() . "\n";
                  return;
				}
              if ($datamart->getHttpRC() != 200) {
                  echo "Error updating subscriber $subscriberid\n";
                  echo "HTTP rc = " . $datamart->getHttpRC() . "\n";
                  return;
			}
			}
        echo "Finished adding new postal mail subscribers\n";
    }

    /*
     *  Returns ID after adding a new row to the subscribers table
     *  If an exact match exists already, it returns the ID of the existing subscriber
     *  Default country is empty string, meaning USA
     */
    public function addSubscriber($firstname,
        $lastname,
        $salutation,
        $address1,
        $address2,
        $city,
        $state,
        $zip,
        $email = "",
        $phone = "",
        $country = "",
        $organization = "",
        $title = "",
        $notes = "")
    {
        // Check for an exact match
        $exactmatch = R::findOne("subscriber", "firstname=? and lastname=? and salutation=? and address1=? and address2=? and city=? and state=? and zip=? and country=? and organization=? and title=? and notes=? and email=? and phone=?",
            array($firstname, $lastname, $salutation, $address1, $address2, $city, $state, $zip, $country, $organization, $title, $notes, $email, $phone));

        // If all fields match exactly those of an existing subscriber
        if ($exactmatch != NULL && $exactmatch != false) {
            // ...just return the id of the existing subscriber
            return $exactmatch->id;
        }

        // otherwise...go ahead and store the subscriber
        $b = R::dispense( 'subscriber' );

        // Prevent Redbean from storing phone number as an int
        $b->setMeta('cast.email', 'string');
        $b->setMeta('cast.phone', 'string');

        $b ->firstname = $firstname;
        $b ->lastname = $lastname;
        $b ->salutation = $salutation;
        $b ->address1= $address1;
        $b ->address2 = $address2;
        $b ->city = $city;
        $b ->state = $state;
        $b ->zip = $zip;
        $b ->country = $country;
        $b ->organization = $organization;
        $b ->title = $title;
        $b ->notes = $notes;
        $b ->email = $email;
        $b ->phone = $phone;

        return R::store($b);
    }

    /*
     *	Implode an array of subscriber fields into a CSV row,
     *  taking into account CSV quoting rules
     */
    public function implodeCSV($fields) {
        $csvrow = "";
        // escape double-quotes
        $copyfields = preg_replace('/"/', '""', $fields);
        // quote any fields with embedded commas or newlines or double-quotes
        foreach ($copyfields as $idx => $field) {
            if (preg_match('/[,"\n]/', $field)) {
                $copyfields[$idx] = "\"$field\"";
            }
        }
        $csvrow = implode(',', $copyfields);
        return $csvrow;
    }

    /*
     *	Explode a subscriber CSV line on commas,
     *  taking into account CSV quoting rules
     *
     *  e.g. Firstname,"Lastname, M.D.",Organization
     *  should not treat the quoted comma as a delimiter
     */
    public function explodeCSV($line) {
        // s - PCRE_DOTALL - dot matches newlines
        // U - PCRE_UNGREEDY - match shortest pattern
        $escapedline = preg_replace_callback('/(^|,)"(.+)"(?=,|$)/sU',
            // remove surrounding quotes, replace escaped double-quotes,
            // and escape commas in any quoted CSV fields
            function ($match) {
                return $match[1] . preg_replace("/,/", "&#44;",
                                       preg_replace('/""/', '"', $match[2])
                                   );
            }, trim($line));

        $boom = explode(',', $escapedline);

        // restore escaped commas in exploded array
        $boom = preg_replace("/&#44;/", ",", $boom);
        return $boom;
    }

    /*
     *  Test if the given CSV row appears to be complete
     *
     *  This is needed to detect CSV rows that contain quoted newlines
     *
     *  Note that we treat CSV whitespace as significant, even if the
     *  data string is later trimmed.  Therefore a CSV with the sequence
     *
     *  field1, "field2"
     *
     *  is not actually quoting field2, and the quotes are literals.
     *  The correct format for this data, quoting field2, is as follows:
     *
     *  field1,"field2"
     *
     *  This matches Excel behavior.
     */
    public function isCompleteCSVRow($row) {
        // Error if final field appears to be quoted but unclosed
        if (preg_match('/(^|,)"([^",]*)$/sU', trim($row))) {
            return False;
        } else {
            return True;
        }
    }

    /*
     *  Split a CSV file into an array of rows,
     *  taking into account quoted newlines
     *  and applying business rules to rearrange fields
     *
     *  Excel converts embedded double-quotes (") into pairs of quotes ("")
     */
    public function getCSVRows($filedata) {
        $filelines = preg_split("/(\r?\n)/", $filedata);
        $rowarray = array();
        $row = "";

        foreach ($filelines as $idx => $line) {
            $row .= $line;
            // If row looks OK, add it to the array of completed CSV rows
            if ($this->isCompleteCSVRow($row)) {
                $row = $this->modifyCSVRow($row);
                $rowarray[] = $row;
                $row = "";
            } else {
                // If row has too many fields, halt processing to prevent
                // partial row from swallowing up the entire input file
                $fields = $this->explodeCSV($row);
                if (count($fields) > CSV_FIELDS_PER_LINE_MAX) {
                    throw new Exception("Too many fields at line $idx");
                }
                $row .= "\n";
            }
        }
        // Chuck a wobble if the final row did not validate
        if ($row != "") {
            throw new Exception("Incomplete final line");
        }
        return $rowarray;
    }

    /*
     *  Business rules for reformatting CSV rows
     */
    public function modifyCSVRow($row) {
        $fields = $this->explodeCSV($row);
        foreach ($fields as $idx => $field) {
            // Trim whitespace from all fields
            $fields[$idx] = trim($fields[$idx]);
        }
        foreach ($fields as $idx => $field) {
            $field_parts = explode("\n", $field);
            switch ($idx) {
                // If address1 is two lines and address2 is empty,
                // split address1 across the two fields
                case CSV_ADDRESS1:
                    if (count($field_parts) == 2) {
                        $address2 = trim($fields[CSV_ADDRESS2]);
                        if (strlen($address2) == 0) {
                            $fields[CSV_ADDRESS1] = $field_parts[0];
                            $fields[CSV_ADDRESS2] = $field_parts[1];
                        }
                    } else if (count($field_parts) > 2) {
                        throw new Exception("Too many lines in field $idx");
                    }
                    break;
                case CSV_NOTES:	// notes, multiple lines OK
                    break;
                default:
                    if (count($field_parts) > 1) {
                        throw new Exception("Too many lines in field $idx");
                    }
            }
        }
        return $this->implodeCSV($fields);
    }

    /*
     *  Validates a batch file to make sure it's ok for importing
     *  Returns debug output, or an empty string on success
     */
    public function validateFileData($filedata) {
        if (strlen($filedata)==0) {
            return "File is empty or inaccessible.";
        }

        $lines = array();
        try {
            $lines = $this->getCSVRows($filedata);
        } catch (Exception $e) {
            return "CSV format error: " . $e->getMessage();
        }
        if (!is_array($lines) || count($lines)==0) {
            return "Could not read data.";
        }

        $i=1;
        foreach ($lines as $line) {
            $csv = $this->explodeCSV($line);
            // skip empty string after trailing delimiter
            if ($i==sizeof($lines) && is_array($csv) && sizeof($csv)==1 && $csv[CSV_ID]=="")
                continue;

            if (!is_array($csv) || sizeof($csv)<CSV_FIELDS_PER_LINE_MIN || sizeof($csv)>CSV_FIELDS_PER_LINE_MAX) {
                return "Line " . $i . " is malformed:<br/><br/>" .
                        "<strong>" . $line . "</strong>" .
                        "<br/><br/>Each line of the import file is expected to have the following format:<br/><br/>" .
                        CSV_FIELD_NAMES_LINE .
                        "<br/><br/>The following fields are tolerated for legacy support but they " .
                        "are ignored by the importer:<br/>" .
                        "Unique ID<br/>email<br/>Phone Number" .
                        "<br/><br/>The following fields are optional:<br/>" .
                        "Country<br/>Organization<br/>Title<br/>Notes<br/>" .
                        print_r($csv, True);
            }
            $i++;
        }
        return "";
    }

    public function addSubscribersFromFileData($listid, $filedata, $skipFirstRow) {
        // Get the list reference
        $list = $this->getList($listid);
        if ($list==NULL) throw new Exception("List with id '$listid' not found.");

        // Load each line of data into a map. There should be one subscriber per line
        $datarows = $this->getCSVRows($filedata);

        // Loop over each line in file
        foreach ($datarows as $row) {
            if ($skipFirstRow) {
                $skipFirstRow=false;
                continue;
            }
            if (strlen($row)==0)
                continue;

            // Get csv values as array
            $csv = $this->explodeCSV($row);

            // make sure we have the correct number of values
            if (sizeof($csv)<CSV_FIELDS_PER_LINE_MIN || sizeof($csv)>CSV_FIELDS_PER_LINE_MAX)
                continue;

            // Remove the garbage fields
            unset($csv[CSV_ID]);

            // If everything is empty, skip this row
            if (strlen(implode("", $csv))==0)
                continue;

            // get optional fields
            $country = (isset($csv[CSV_COUNTRY])) ? $csv[CSV_COUNTRY] : "";
            $organization = (isset($csv[CSV_ORGANIZATION])) ? $csv[CSV_ORGANIZATION] : "";
            $title = (isset($csv[CSV_TITLE])) ? $csv[CSV_TITLE] : "";
            $notes = (isset($csv[CSV_NOTES])) ? $csv[CSV_NOTES] : "";

            // Add a subscriber to the database
            $id = $this->addSubscriber(trim($csv[CSV_FIRST_NAME]), // firstname
                trim($csv[CSV_LAST_NAME]), // lastname
                "", // salutation not contained in CSV
                trim($csv[CSV_ADDRESS1]), // address1
                trim($csv[CSV_ADDRESS2]), // address2
                trim($csv[CSV_CITY]), // city
                trim($csv[CSV_STATE]), // state
                trim($csv[CSV_ZIP]), // zip
                trim($csv[CSV_EMAIL]), // email
                trim($csv[CSV_PHONE]), // phone
                trim($country), // country
                trim($organization), // organization
                trim($title), // title
                trim($notes)); // notes

            // Get a managed reference to the database entity
            $subscriber = $this->getSubscriber($id);

            // Link the subscriber to the list
            $this->link($subscriber, $list);
        }
    }

    public function getSubscriber($id) {
        $s = R::load('subscriber', $id);
        if ($s->id==0)
            return NULL;
        return $s;
    }

    /*
     *  All subscribers to a given mailing list
     */
    public function getSubscribersByList($listid) {
        $list = $this->getList($listid);
        if ($list==NULL) {
            return array();
        }
        $subs = R::related( $list, 'subscriber');
        usort($subs, 'sortSubscribers');
        return $subs;
    }


    public function getCategorySubscribersByList($listid) {
        // Get the list from the list ID
        $list = $this->getList($listid);
        if ($list == NULL) {
            return array();
        }
        // Get the categories that have been associated with this list in GovDelivery
        $categories = R::related( $list, 'category');
        if ($categories == NULL || sizeof($categories) == 0) {
            return array();
        }
        // Get the subscribers that are associated with the categories that are associated with this list
        $cat_subs = array();
        foreach ($categories as $cat) {
            // Get subscribers to this category
            $subs = R::related( $cat, 'subscriber');
            if ($subs == NULL || sizeof($subs)==0) {
                continue;
            }
            // Loop over the subscribers to check each one individually
            foreach ($subs as $sub) {
                $already_pushed = FALSE;
                foreach ($cat_subs as $cs) {
                    if ($cs->id == $sub->id) {
                        // The response list already contains this subscriber
                        $already_pushed = TRUE;
                    }
                }
                if (!$already_pushed) {
                    // Add the subscriber to the response list
                    array_push($cat_subs, $sub);
                }
            }
        }

        // Remove direct subscribers
        $direct_subs = $this->getSubscribersByList($listid);
        $rval = array();
        foreach ($cat_subs as $cs) {
            $to_include = true;
            foreach ($direct_subs as $ds) {
                if ($ds->id == $cs->id) {
                  $to_include = false;
                  break;
                }
            }
            if ($to_include == true){
            	array_push($rval, $cs);
            }
        }

        usort($rval, 'sortSubscribers');
        return $rval;
    }


    /*
     *  Returns an array of the id's of each list that the
     *  specified subscriber is subscribed to
     */
    public function getSubscriptions($subscriber) {
        $rval = array();
        $lists = R::related( $subscriber, 'list');
        usort($lists, "sortLists");
        foreach ($lists as $l) { // loop associated lists
            if (MLM::hasAccessToUnit($l->unitid))
                array_push($rval, $l->id);
        }
        return $rval;
    }

    /*
     *  Copy subscribers from one list to another
     */
    public function copySubscribers($fromListId, $toListId) {
        $fromList = $this->getList($fromListId);
        $toList = $this->getList($toListId);

        if ($fromList==NULL)
            throw new Exception('Could not copy from nonexistent list');
        if ($toList==NULL)
            throw new Exception('Could not copy to nonexistent list');

        $subscribers = $this->getSubscribersByList($fromListId);
        foreach ($subscribers as $sub) {
            $this->link($toList, $sub);
        }
    }


    /*
     *  Delete all subscribers from list (by id)
     */
    public function deleteAllSubscribersFromList($listId) {
        $list = $this->getList($listId);
        if ($list !== NULL) {
            $subscribers = $this->getSubscribersByList($listId);
            foreach ($subscribers as $sub) {
                # $name = $sub->lastname . ", " . $sub->firstname;
                # echo "Unsubscribing $name\n";
                $this->unlink($list, $sub);
            }
        }
    }


    public function isSubscribedToCategory($subid, $catcode) {
        $subscriber = $this->getSubscriber($subid);
        if ($subscriber == NULL)
            return FALSE;
        $category = $this->getCategory($catcode);
        $categories = R::related( $subscriber, 'category' );
        $linked = FALSE;
        foreach ($categories as $cat) {
            if (strcmp($catcode, $cat->code)==0)
                $linked = TRUE;
        }
        return $linked;
    }



    // --------------------------
    //   Categories
    // --------------------------


    public function getCategories() {
        $cats = R::find('category', '', array());
        $rval = array();
        foreach ($cats as $c) {
            if (MLM::hasAccessToUnit($c->code))
                array_push($rval, $c);
				}
        usort($rval, 'sortCategories');
        return $rval;
    }


    public function getCategory($code) {
        if (!MLM::hasAccessToUnit($code))
            return NULL;

        return R::findOne('category', 'code=?', array($code));
    }

    public function getSubCategories($category) {
        $rval = array();
        $cats = R::find('category', 'parent_code=?', array($category->code));
        foreach ($cats as $c) {
            if (MLM::hasAccessToUnit($c->code))
                array_push($rval, $c);
						}

        usort($rval, 'sortCategories');
        return $rval;
    }



    /*
     *  Returns ID after adding a new row to the database
     *  Returns existing ID if code is already in use
     *
     *  Not limited by user privileges. Should be called only by admin/script
     */
    public function mergeCategory($code, $parent_code, $allow_subscriptions, $default_open, $description, $long_name, $short_name, $uri) {
        // code must be unique
        $b = R::findOne('category', 'code=?', array($code));
        if ($b == null) {
            // new category
            $b = R::dispense('category');
        }

        $b->code = $code;
        $b->parent_code = $parent_code;
        $b->allow_subscriptions = ($allow_subscriptions=="true");
        $b->default_open = $default_open;
        $b->description = $description;
        $b->long_name = $long_name;
        $b->short_name = $short_name;
        $b->uri = $uri;

        return R::store($b);
    }


    public function syncCategories() {
        $xml = gd_get_all_categories_xml();

        // Get a list of categories from GovDelivery
        $gd_cats = gd_get_categories_as_array($xml);

        // Can't proceed without GD categories
        if (!sizeof($gd_cats)>0)
            return;

        // Look for new/updated categories
        foreach ($gd_cats as $cat) {
            // Ignore the LOCKFILE category
            if (strcmp($cat["code"],'LOCKFILE')==0)
                continue;
            // Store the new/update category in the database
            $this->mergeCategory(
                $cat["code"],
                $cat["parent_code"],
                $cat["allow_subscriptions"],
                $cat["default_open"],
                $cat["description"],
                $cat["long_name"],
                $cat["short_name"],
                $cat["uri"]
            );
        }

        // Get a map of { topic => { associated categories } }
        $topic_cats = gd_get_topic_cats_map($xml);

        // Associate lists and categories
        foreach ($topic_cats as $topic=>$cats_map) {
            // Look up the mlm list based on the topic code
            $list = $this->getListByTopic($topic);
            if ($list == null)
                continue;

            $this->beginTrans();
            try {
                // Remove old list/category relationships
                $this->clearLinks($list, "category");
                foreach ($cats_map as $cat_code) {
                    $cat = $this->getCategory($cat_code);
                    // Create new list/category relationships
                    if ($cat != null)
                        $this->link($list, $cat);
                }
                $this->commitTrans();
            }
            catch(Exception $e) {
                $this->rollbackTrans();
                echo "Failed to update categories for list with topic id '" . $topic . "'.";
            }
        }
    }


    // --------------------------
    //   Session Management
    // --------------------------


    /*
     *  Returns true on success, false otherwise
     */
    public static function login($user, $pass) {
    	$ROLES_TO_IGNORE = array(ANONYMOUS_ROLE);
        if ($user=='testytester') {
				$datamart = new DataMartStub();
        } else {
				$datamart = new DataMart(DM_USER, DM_PASS, DM_PATH);
			}
        $dm_userdata = null;
        try {
            $dm_userdata = $datamart->getXml("/users/$user");
        } catch (Exception $e) {
            return false;
        }
        if ($dm_userdata === null) { return false; }
        $dm_userdata = trim($dm_userdata);
        if ($dm_userdata === "") { return false; }

				// If user exists, generate sham password
        $shamuser = "${user}MLMLOGIN";
        $shampw = md5($shamuser);

				// If sham password matches, user is OK, create session
        if ($shampw !== $pass) { return false; }

				// Create array of units whose mailing lists this user can administer
        $isNationalAdmin = False;
        try {
            $xml = new SimpleXmlElement($dm_userdata);
				$unitidlist = array();
            foreach ($xml->unitroles->unitrole as $unitrole) {
                $unitid = strval($unitrole->unit);
                $roleid = strval($unitrole->role);
                // Allow District-level users to access all mailing lists at
                // the Forest level
					$forestunitid = substr($unitid, 0, 6);

                // Only add unit if the role is not on the ignore list
                if (array_search($roleid, $ROLES_TO_IGNORE, True) == False) {
                    $unitidlist[$forestunitid] = True;
                    if (preg_match("/^110*$/", $unitid) > 0) {
                        $isNationalAdmin = True;
						}
					}
				}
				$units = array_keys($unitidlist);	// remove duplicates
        } catch (Exception $e) {
				return false;
			}
        if (isset($units) && is_array($units) && sizeof($units)>0) {
				@session_start();
				$_SESSION['user'] = $user;
				$_SESSION['units'] = $units;
				$_SESSION['nationalAdmin'] = $isNationalAdmin;
				return true;
        } else {
				return false;
			}
    }

    public static function logout() {
        $SESSION_NAME = session_name();

        if ( isset($_COOKIE[$SESSION_NAME]) ) {
            session_id($_COOKIE[$SESSION_NAME]);

            @session_start();
            session_destroy();

            if (!headers_sent()) {
                setcookie(session_name(), '', time() - 3600);
            }
            unset($_COOKIE[session_name()]);
        }
    }

    public static function getToken() {
        return session_id();
    }

    /*
     *  Code to determine if the user is logged in properly and to restore
     *  the session
     *
     *  Note that this will give precedence to a session token supplied in a URL
     *  parameter as "token=?" over a browser cookie.  This lets the PALS login
     *  work consistently, but is a bad security practice.
     */
    public static function checkSession() {
        $SESSION_NAME = session_name();
        $my_session_id = "";

        if (isset($_GET[TOKEN_NAME])) {
            $my_session_id = $_GET[TOKEN_NAME];
            session_id($my_session_id);

            @session_start();
        } else if (isset($_COOKIE[$SESSION_NAME])) {
            $my_session_id = $_COOKIE[$SESSION_NAME];
            session_id($my_session_id);
            @session_start();
        }

        if (isset($_SESSION['user'])) {
            // User is logged in
        } else {
            // User is not logged in
            header("Location: login-error.php");
            exit(0);
        }
    }

    public static function hasAccessToUnit($unitid) {
        $normalizedunit = MLM::normalizeUnitID($unitid);
        $unitidarray = $_SESSION['units'];
        foreach ($unitidarray as $idx) {
            $idxn = MLM::normalizeUnitID($idx);
            if (substr($normalizedunit, 0, strlen($idxn))==$idxn) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function getLastListId() {
        if (!isset($_SESSION['recentlists'])) {
            $_SESSION['recentlists'] = array();
        }
        $recentlists = $_SESSION['recentlists'];
        if (sizeof($recentlists)>0) {
            return $_SESSION['recentlists'][0];
        }
        return FALSE;
    }

    public static function getBulletinDetails($bulletin_id) {
    	return simplexml_load_string(gd_get_bulletin_details_xml($bulletin_id));
    }

    public static function createBulletin($draft_id, $bbody, $bsubject, $btopic, $filename, $remove) {

    	if (strlen($draft_id)==0) {
	    	$result = simplexml_load_string(gd_create_bulletin($bbody, $bsubject, $btopic, $filename));

    		$new_bulletin_id = $result->{"to-param"};
    		$subscribers = $result->{"total-subscribers"};

	    	$BulletinQuery = "INSERT INTO bulletin(bulletin_id,subject,recipient_count,sent) VALUES(?,?,?,now())";
    		R::exec( $BulletinQuery, array($new_bulletin_id, $bsubject, $subscribers));

    		$BulletinTopicQuery = "INSERT INTO bulletin_topic(bulletin_id,topic_code) VALUES(?,?)";
	    	R::exec( $BulletinTopicQuery, array($new_bulletin_id, $btopic));
    	}
    	else
    	{
    		$result = MLM::updateDraft($draft_id, $bbody, $bsubject, $btopic, $filename, $remove);

    		$result = simplexml_load_string(gd_send_draft($draft_id, $bbody, $bsubject, $btopic));
    		$subscribers = $result->{"total-subscribers"};

    		$DraftQuery = "UPDATE bulletin SET subject=?, recipient_count =?, sent=now() WHERE bulletin_id=?";
    		R::exec( $DraftQuery, array($bsubject, $subscribers, $draft_id));
    	}

    	return $result;
    }

    public function manageDraft($draft_id, $bbody, $bsubject, $btopic, $filename, $remove, $email) {

    	if (strlen($draft_id)==0) {
    		$result = simplexml_load_string(gd_create_draft($bbody, $bsubject, $btopic, $filename));

    		$draft_id = $result->{"to-param"};
    		$subscribers = $result->{"total-subscribers"};

    		$DraftQuery = "INSERT INTO bulletin(bulletin_id,subject,recipient_count,sent) VALUES(?,?,?,NULL)";
    		R::exec( $DraftQuery, array($draft_id, $bsubject, $subscribers));

    		$DraftTopicQuery = "INSERT INTO bulletin_topic(bulletin_id,topic_code) VALUES(?,?)";
    		R::exec( $DraftTopicQuery, array($draft_id, $btopic));

    		if ($filename != '')
    		{
    			$result = simplexml_load_string(gd_post_file($draft_id, $filename));
    			$attachment_id = $result->{"to-param"};

    			$DraftAttachmentQuery = "INSERT INTO bulletin_attachment(bulletin_id,attachment_id) VALUES(?,?)";
    			R::exec( $DraftAttachmentQuery, array($draft_id, $attachment_id));
    		}
    	}
    	else {
    		$result = $this->updateDraft($draft_id, $bbody, $bsubject, $btopic, $filename, $remove);

    		$DraftQuery = "UPDATE bulletin SET subject=?,created=now() WHERE bulletin_id=?";
    		R::exec( $DraftQuery, array($bsubject, $draft_id));
    	}

    	if (strlen($email)>0)
    	{
    		gd_send_test_email($draft_id, $email);
    	}

    	return $result;
    }

    public function updateDraft($draft_id, $bbody, $bsubject, $btopic, $filename, $remove){
    	$result = simplexml_load_string(gd_update_draft($draft_id, $bbody, $bsubject, $btopic, $filename));

    	if ($remove==1 || $filename != '')
    	{
    		$sql = R::getAll('SELECT * FROM bulletin_attachment WHERE bulletin_id=? LIMIT 1', array($draft_id));
    		$attachments = R::convertToBeans('attachments',$sql);

    		foreach ($attachments as $attachment) {
    			$attachment_id = $attachment->attachment_id;
    			break;
    		}

    		if (strlen($attachment_id)>0){
    			gd_delete_attachment($draft_id, $attachment_id);

    			$DeleteAttachmentQuery = "DELETE FROM bulletin_attachment WHERE bulletin_id=? AND attachment_id=?";
    			R::exec($DeleteAttachmentQuery, array($draft_id, $attachment_id));
    		}
    	}

    	if ($filename != '')
    	{
    		$result = simplexml_load_string(gd_post_file($draft_id, $filename));
    		$attachment_id = $result->{"to-param"};

    		$DraftAttachmentQuery = "INSERT INTO bulletin_attachment(bulletin_id,attachment_id) VALUES(?,?)";
    		R::exec( $DraftAttachmentQuery, array($draft_id, $attachment_id));
    	}

    	return $result;
    }

    public static function getBulletins($topic_code){
    	$result = R::getAll('select * from bulletin b JOIN bulletin_topic t ON b.bulletin_id=t.bulletin_id where sent IS NOT NULL AND topic_code=?', array($topic_code));
    	$bulletins = R::convertToBeans('bulletins',$result);

    	return $bulletins;
    }

    public static function getDrafts($topic_code){
    	$result = R::getAll('select * from bulletin b JOIN bulletin_topic t ON b.bulletin_id=t.bulletin_id where sent IS NULL AND topic_code=?', array($topic_code));
    	$drafts = R::convertToBeans('drafts', $result);

    	return $drafts;
    }
		}

    function sortLists($a, $b) {
        return (strtoupper($a->name) < strtoupper($b->name)) ? -1 : 1;
		}

    function sortSubscribers($a, $b) {
        $al = strtoupper($a->lastname);
        $bl = strtoupper($b->lastname);
        if ($al == $bl) {
            return (strtoupper($a->firstname) < strtoupper($b->firstname)) ? -1 : 1;
			}
        return ($al < $bl) ? -1 : 1;
    }

    function sortCategories($a, $b) {
        return (strtoupper($a->short_name) < strtoupper($b->short_name)) ? -1 : 1;
		}


?>
