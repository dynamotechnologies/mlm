<?php

class DataMart
{
	//some basic config information for record types.
	private static $subscriber_fields = array('subscriberid', 'lastname', 'firstname', 'caraprojectid',
		'title', 'org', 'orgtype', 'subscribertype', 'addstreet1', 'addstreet2', 'city', 'zip', 'state',
		'prov', 'country', 'email', 'phone', 'contactmethod', 'subscribeddate');

    private $uid;
    private $pwd;
    private $path;
    private $isCacheEnabled;
    private $cacheDir;
    private $cacheTimeout;
    private $lastHttpStatus = NULL;

    public function DataMart($uid, $pwd, $path, $cacheenabled=false, $cachedir="/tmp", $cachetimeout=1800) {
        $this->uid = $uid;
        $this->pwd = $pwd;
        $this->path = $path;
        $this->isCacheEnabled = $cacheenabled;
        $this->cacheDir = $cachedir;
        $this->cacheTimeout = $cachetimeout;
    }

    public function getXml($path, $skipcache=false) {
        if (substr($path, 0, 4)=="http" && $skipcache==false) { // forcibly bypass cache if fully qualified path specified.
            return $this->getXml($path, true); // ...mostly to avoid rewriting the logic that determines the cache file name
        }
        $xml = "";
        $cacheFileName = str_replace("/", "_", $path);
        $cacheFileName = str_replace("?", "_", $cacheFileName);
        $cacheFileName = str_replace("=", "_", $cacheFileName);
        $cacheFileName = $this->cacheDir . "/" . $cacheFileName . ".tmp";
        if ($this->isCacheEnabled && is_readable($cacheFileName) && !($skipcache) && // is_readable will fail if the cache file doesn't exist
            (time()-filemtime($cacheFileName)<$this->cacheTimeout) ) {
            $xml = @file_get_contents($cacheFileName);
            if ($xml==FALSE || !(strlen($xml)>0)) {
                $xml = $this->getXml($path, true);
            }
        } else {
            $context = stream_context_create(array(
                'http' => array(
                    'header'  => "Authorization: Basic " . base64_encode($this->uid.":".$this->pwd)
                )
            ));
            if (substr($path, 0, 4)!="http") { // allow fully qualified paths
                $path = $this->path . $path;
            }
            $xml = @file_get_contents($path, false, $context);
            $this->lastHttpStatus = $http_response_header[0];
            if ($xml==FALSE) {
                $xml = "";
            }
            if ($this->isCacheEnabled && strlen($xml)>0 && !($skipcache)) {
                $cfile = fopen($cacheFileName, "w");
                fwrite($cfile, $xml);  // Write to cache
                fclose($cfile);
            }
        }
        return $xml;
    }

    /*
     *  Send an XML string to a URL using HTTP PUT
     *  Check response code using getHttpRC()
     *
     *  For some inexplicable reason, the right way to do this in PHP is
     *  with file_get_contents(), rather than file_put_contents()
     */
    public function putXml($path, $xml) {
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'PUT',
                'header' => "Content-Type: application/xml\r\n" .
                            'Authorization: Basic ' .
                             base64_encode($this->uid.":".$this->pwd)."\r\n",
                'content' => $xml
            )
        ));
        if (substr($path, 0, 4)!="http") { // allow fully qualified paths
            $path = $this->path . $path;
        }
        $resp = @file_get_contents($path, false, $context);
        $this->lastHttpStatus = $http_response_header[0];
    }

    /*
     *  Send a DELETE request
     */
    public function delete($path) {
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'DELETE',
                'header' => 'Authorization: Basic ' .
                             base64_encode($this->uid.":".$this->pwd)."\r\n"
            )
        ));
        if (substr($path, 0, 4)!="http") { // allow fully qualified paths
            $path = $this->path . $path;
        }
        $resp = @file_get_contents($path, false, $context);
        $this->lastHttpStatus = $http_response_header[0];
    }

    /*
     *  Return the HTTP response code for the previous operation
     *  If there was no previous operation, this returns NULL
     */
    public function getHttpRC() {
        if ($this->lastHttpStatus != NULL) {
            $status_parts = explode(" ", $this->lastHttpStatus);
            return $status_parts[1];
        } else {
            return NULL;
        }
    }

	//we could potentially return an entire user record here, but
	//we only need the role information for now.  Should we
	//need more write a new function and have this one call it and
	//return the necesary subset
	//
	// returns array('unitid' => array(role1onunit, role2onunit, ...))
	public function getUserRoles($user)
	{
		$dm_userdata = $this->getXml("/users/$user");

		//rely on the fact that trim(null) === ""
		$dm_userdata = trim($dm_userdata);
		if ($dm_userdata === "")
		{
			throw new Exception("No data returned for user '$user'");
		}

		$roles = array();
		$xml = new SimpleXMLElement($dm_userdata);
		foreach($xml->unitroles->unitrole as $unitrole)
		{
			$unitid = strval($unitrole->unit);
			$roleid = strval($unitrole->role);

			if (isset($roles[$unitid]))
			{
				$roles[$unitid][] = $roleid;
			}
			else
			{
				$roles[$unitid] = array($roleid);
			}
		}

		return $roles;
	}

	public function getPalsIdFromCaraId($caraprojid)
	{
		$caraproj_xml = $this->getXml("/caraprojects/$caraprojid");
		if ($caraproj_xml == null)
		{
			throw new Exception("Error getting CARA project rec for ID $caraprojid: HTTP rc = " . $this->getHttpRC());
		}

		$caraproj = new SimpleXmlElement($caraproj_xml);
		return strval($caraproj->palsid);
	}

	public function getUnsubscribed($contactMethod)
	{
		if(!in_array($contactMethod, array("", "Email", "Mail" )))
		{
			throw new Exception("Invalid contact method $contactMethod");
		}

		$url = "/utils/subscribers/unsubscribed?activetopic=1";
		if ($contactMethod)
		{
			$url .= "&contactmethod=$contactMethod";
		}

		// only look at pending subscriptions for active mailing lists
		$subscribers_xml = $this->getXml($url);
		if ($subscribers_xml == NULL)
		{
			throw new Exception("Error getting list of new subscribers: HTTP rc = " . $this->getHttpRC());
		}

		$subscribers = new SimpleXmlElement($subscribers_xml);
		$results = array();
  	foreach ($subscribers as $sub)
		{
			$results[] = $this->subscriberToArray($sub);
		}
		return $results;
	}

	public function updateSubscriber($subarray)
	{
		$xml = $this->subscriberToXml($subarray);
		$subscriberURL = "/caraprojects/$subarray[caraprojectid]/mailinglist/subscribers/$subarray[subscriberid]";
		$this->putXml($subscriberURL, $xml);

		if ($this->getHttpRC() != 200)
		{
			throw new Exception("Error getting list of new subscribers: HTTP rc = " . $this->getHttpRC());
		}
	}

	private function subscriberToArray($sub)
	{
		$subarray = array();
		foreach(self::$subscriber_fields AS $field)
		{
			if (isset($sub->$field))
			{
				$subarray[$field] = trim(strval($sub->$field));
			}
			else
			{
				$subarray[$field] = null;
			}
		}
		return $subarray;
	}

	private function subscriberToXml($subarray)
	{
		$xml = new XmlWriter();
		$xml->openMemory();
		$xml->startDocument('1.0','UTF-8');
		$xml->startElement('subscriber');

		//safety tip: writing attributes after child elements probably won't work
		$xml->writeAttribute('xmlns', 'http://www.fs.fed.us/nepa/schema');

		foreach(self::$subscriber_fields AS $field)
		{
			//the schema doesn't like a null date here, but will accept a missing element
			if($field == 'subscribeddate' AND empty($subarray[$field]))
			{
				continue;
			}
			$xml->writeElement($field, $subarray[$field]);
		}

		$xml->endElement();
		$xml->endDocument();
		return $xml->outputMemory();
	}
}

//extend to pick up higher level functions while stubbing out
//anything that would actually attempt to call the web.
class DataMartStub extends DataMart
{
	//intentionally don't call parent constructor.
	public function __construct() {}

	public function getXml($path, $skipcache=false)
	{
		if (strpos($path,'/users/')===0)
		{
			return
				"<user xmlns=\"http://www.fs.fed.us/nepa/schema\">" .
					"<shortname>testytester</shortname>" .
					"<firstname>Testy</firstname>" .
					"<lastname>Tester</lastname>" .
					"<email>testytester@phaseonecg.com</email>" .
					"<phone>301-867-5309</phone>" .
					"<title/>" .
					"<unitroles><unitrole><unit>1104</unit><role>1</role></unitrole></unitroles>" .
				"</user>";
		}

		throw new Exception("DataMart Stub object does not recognize path '$path'.");
	}

	public function putXml($path, $xml)
	{
		throw new Exception('Datamart Sub does not implement putXml');
	}

	public function delete($path)
	{
		throw new Exception('Datamart Sub does not implement delete');
	}

	public function getHttpRC()
	{
		throw new Exception('Datamart Sub does not implement getHttpRC');
	}
}

class DataMartStub
{
    public function DataMartStub() {
    }

    public function getXml($path, $skipcache=false) {
        if (strpos($path,'/users/')===0) {
            return "<user xmlns=\"http://www.fs.fed.us/nepa/schema\">" .
                        "<shortname>testytester</shortname>" .
                        "<firstname>Testy</firstname>" .
                        "<lastname>Tester</lastname>" .
                        "<email>testytester@phaseonecg.com</email>" .
                        "<phone>301-867-5309</phone>" .
                        "<title/>" .
                        "<unitroles><unitrole><unit>1104</unit><role>1</role></unitrole></unitrole></unitroles>" .
                    "</user>";
        }
        throw new Exception("DataMart Stub object does not recognize path '$path'.");
    }
}
?>
