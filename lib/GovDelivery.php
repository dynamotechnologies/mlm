<?php

/*
 * Utility functions for accessing GovDelivery web APIs
 */

require_once("MLM-CONF.php");

/*
 *  Get full list of active topics from GovDelivery
 *  Return as a hash of arrays containing topic info
 *  using topicid as key
 *
 *  Topic info consists of:
 *    * topicid
 *    * name (shortname)
 *
 *  Omit topics that appear to be inactive (NEPA_XXX without a trailing _S)
 *  Accept all other topics
 */
function gd_get_topics() {
    $xml = gd_get_topics_xml();
    $gd_topics = new SimpleXmlElement($xml);
    $result = array();
    foreach ($gd_topics->topic as $gd_topic) {
      $topic = array();
      $topicid = strval($gd_topic->code);
      $topic['topicid'] = $topicid;
      $shortnametag = 'short-name';
      $topic['name'] = strval($gd_topic->$shortnametag);
      if (preg_match("/^NEPA_/", $topicid) == 1) {
        if (preg_match("/_S$/", $topicid) == 0) {
          continue;
        }
      }
      $result[$topicid] = $topic;
    }
    return $result;
}

/*
 *  Get full list of topics from GovDelivery as XML
 */
function gd_get_topics_xml() {
    return http_get("topics.xml");
}

/*
 *  Get unit ID corresponding to the topic
 */
function gd_get_unitid($topicid) {
    $ACCOUNT = GD_ACCT;

    $xml = gd_get_categories($topicid);
    $gd_categories = new SimpleXmlElement($xml);
    $categorylist = $gd_categories->category;
    if (count($categorylist) == 0) {
      throw new Exception("No categories for topic $topicid");
    }

    // get details for first category
    $categoryuritag = "category-uri";
    $uri = $categorylist[0]->$categoryuritag;
    // remove leading "/api/account/USDAFS/"
    $pattern = '/^\/api\/account\/' . $ACCOUNT . '\/(.*)/';
    $url = preg_replace($pattern, '${1}', $uri);
    $xml = http_get($url);
    $gd_category = new SimpleXmlElement($xml);

    $code = strval($gd_category->code);
    $parent = $gd_category->parent;
    $maxdepth = 25;

    // if category has a parent node,
    // climb up the category hierarchy to the root
    // (actually 1 lvl down from the global root)
    while ((bool)$parent != False) {
      if ($maxdepth <= 0) {
        throw new Exception("Error finding root category for topic $topicid");
      }
      $code = strval($parent->code);
      $xml = gd_get_category($code);
      $gd_category = new SimpleXmlElement($xml);
      $maxdepth = $maxdepth - 1;
      $code = strval($gd_category->code);
      $parent = $gd_category->parent;
    }
    if (is_null($code)) {
      throw new Exception("Unit ID for topic $topicid is null, is category linked correctly?");
    }
    if (!is_numeric($code)) {
      throw new Exception("Unit ID for topic $topicid is non-numeric ($code), is root category coded correctly?");
    }

    $unitid = intval($code);
    return $unitid;
}

/*
 *  Get details for single category as XML
 */
function gd_get_category($categoryid) {
    return http_get("categories/${categoryid}.xml");
}

/*
 *  Get list of categories for topicid as XML
 */
function gd_get_categories($topicid) {
    return http_get("topics/$topicid/categories.xml");
}


/*
 *  Get xml of entire category hierarchy
 */
function gd_get_all_categories_xml() {
 //   return http_get("category_hierarchy");
    return http_get("categories.xml");
}


/*
 *  Get array of all categories
 */
function gd_get_categories_as_array($xml) {
    $gd_category_tree = new SimpleXmlElement($xml);

    $fpg = fopen("/var/tmp/gd_get_categories_as_array.log", "w");
  //  fwrite($fpg, "$xml \n");

$ct = $gd_category_tree->count();

fwrite($fpg, "Count = $ct \n Now children = \n");

foreach ($gd_category_tree->children() as $somecat){
	$code = $somecat->code;
	fwrite($fpg, " $code\n");
}


    fwrite($fpg, "Done \n");
    fclose($fpg);

//    return gd_flatten_cats($gd_category_tree->categories->category);
    return gd_flatten_cats($gd_category_tree->children());
}


function gd_get_topic_cats_map($xml) {
    $gd_category_tree = new SimpleXmlElement($xml);
    return gd_flatten_topics($gd_category_tree->categories->category);
}


function gd_flatten_cats($category, $parent_code=NULL) {
    $rval = array();

    $fp = fopen("/var/tmp/gd_flatten_cats.log", "w");
    fwrite($fp, "Start \n");

    foreach ($category as $cat) {
        // Extract category attributes
        $code = (string) $cat->code;
    	fwrite($fp, "Code: $code \n");
        $allow_subscriptions = (string) $cat->{'allow-subscriptions'};
        $default_open = (string) $cat->{'default-open'};
        $description = (string) $cat->description;
        $long_name = (string) $cat->name;
        $short_name = (string) $cat->{'short-name'};
        $uri = (string) $cat->link['href'];

        // Add this category to the response
        $rval[] = array(
            "code"=>$code,
            "parent_code"=>$parent_code,
            "allow_subscriptions"=>$allow_subscriptions,
            "default_open"=>$default_open,
            "description"=>$description,
            "long_name"=>$long_name,
            "short_name"=>$short_name,
            "uri"=>$uri
        );

        if (isset($cat->categories->category)) {
            $sub_cats = gd_flatten_cats($cat->categories->category, $code);
            $rval = array_merge($rval, $sub_cats);
        }
        fwrite($fp, "Done with this code \n");
    }
    fwrite($fp, "Finished");
    fclose($fp);
    return $rval;
}

/*
 * Returns a map of categories indexed by topic. For example:
 *   array(topicA => array(cat1, cat2, cat3),
 *         topicB => array(cat3, cat4, cat9) )
 */
function gd_flatten_topics($category) {
    $rval = array();

    foreach ($category as $cat) {
        // Extract category attributes
        $cat_code = (string) $cat->code;
        if (isset($cat->topics->topic)) {
            foreach ($cat->topics->topic as $topic) {
                if (!isset($rval[(string)$topic->code])) {
                    $rval[(string)$topic->code] = array($cat_code);
                } else {
                    array_push($rval[(string)$topic->code], $cat_code);
                }
            }
        }

        // Extract topics from the subcategories
        if (isset($cat->categories->category)) {
            $sub_topics = gd_flatten_topics($cat->categories->category);
            foreach ($sub_topics as $key=>$val) {
                if (!isset($rval[$key])) {
                    $rval[$key] = $val;
                } else {
                    foreach ($val as $v) {
                        array_push($rval[$key], $v);
                    }
                }
            }
        }
    }

    return $rval;
}


function http_get($rsrc) {
    $HOST = GD_HOST;
    $ACCOUNT = GD_ACCT;
    $USERPWD = GD_USERPW;

    $ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
    $AUTH_USERPWD = utf8_encode($USERPWD);

    $url = "$ACCOUNT_PREFIX/$rsrc";

    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // Return a variable instead of posting it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $AUTH_USERPWD);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);
    $httpcode = $info["http_code"];
    curl_close($ch);
    if ($httpcode != 200) {
      throw new Exception("Error getting GovDelivery resource $rsrc $httpcode");
    } else {
      return $result;
    }
}

function http_delete($rsrc)
{
	$HOST = GD_HOST;
	$ACCOUNT = GD_ACCT;
	$USERPWD = GD_USERPW;

	$ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
	$AUTH_USERPWD = utf8_encode($USERPWD);

	$url = "$ACCOUNT_PREFIX/$rsrc";

	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $AUTH_USERPWD);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	$result = curl_exec($ch);
	//$this->checkErrors($ch, array(200), $result);
	curl_close($ch);
	return $result;
}

function http_put_xml($rsrc, $payload)
{
	$HOST = GD_HOST;
	$ACCOUNT = GD_ACCT;
	$USERPWD = GD_USERPW;

	$ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
	$AUTH_USERPWD = utf8_encode($USERPWD);

	$url = "$ACCOUNT_PREFIX/$rsrc";

	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	// Return a variable instead of posting it directly
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_PUT, true);
	curl_setopt ($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
	$putData = tmpfile();
	fwrite($putData, $payload);
	fseek($putData, 0);
	curl_setopt($ch, CURLOPT_INFILE, $putData);
	curl_setopt($ch, CURLOPT_INFILESIZE, strlen($payload));
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $AUTH_USERPWD);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	$result = curl_exec($ch);
	//$this->checkErrors($ch, array(200), $result);
	curl_close($ch);
	return $result;
}

function http_post_xml($rsrc, $payload)
{
	$HOST = GD_HOST;
	$ACCOUNT = GD_ACCT;
	$USERPWD = GD_USERPW;

	$ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
	$AUTH_USERPWD = utf8_encode($USERPWD);

	$url = "$ACCOUNT_PREFIX/$rsrc";

	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	// Return a variable instead of posting it directly
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml"));
	curl_setopt($ch, CURLOPT_POSTFIELDS, "$payload");
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $AUTH_USERPWD);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	$result = curl_exec($ch);

	//$info = curl_getinfo($ch);
	//$httpcode = $info["http_code"];
	//echo makeErrorMsg($httpcode, $result),
	//var_dump($info);

	curl_close($ch);
	return $result;
}

function makeErrorMsg($code, $svrResponse) {
	$msg = "";
	try {
		$xml = new SimpleXMLElement($svrResponse);
		foreach ($xml->error as $errtxt) {
			$msg = $msg . "\n" . trim($errtxt);
		}
	} catch (Exception $e) { }
	if (strlen($msg) > 0) {
		return $msg;
	} else {
		return "HTTP error $code";
	}
}

function http_post_with_attachment($rsrc, $bbody, $bsubject, $btopic, $filename)
{
	$HOST = GD_HOST;
	$ACCOUNT = GD_ACCT;
	$USERPWD = GD_USERPW;

	$ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
	$AUTH_USERPWD = utf8_encode($USERPWD);

	$url = "$ACCOUNT_PREFIX/$rsrc";

	$fields = array(
			'bulletin[body]'=>$bbody,
			'bulletin[subject]'=>$bsubject,
			'bulletin[topics][][code]'=>$btopic,
			'bulletin[bulletin_files][][deliver]'=>true,
			'bulletin[bulletin_files][][attachment]'=> '@' . GD_UPLOAD_FOLDER . $filename
	);

	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	// Return a variable instead of posting it directly
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: multipart/form-data"));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $AUTH_USERPWD);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	$result = curl_exec($ch);

	curl_close($ch);
	return $result;
}

function http_post_file($rsrc, $filename)
{
	$HOST = GD_HOST;
	$ACCOUNT = GD_ACCT;
	$USERPWD = GD_USERPW;

	$ACCOUNT_PREFIX = "https://${HOST}/api/account/${ACCOUNT}";
	$AUTH_USERPWD = utf8_encode($USERPWD);

	$url = "$ACCOUNT_PREFIX/$rsrc";

	$fields = array(
			'bulletin_file[deliver]'=>true,
			'bulletin_file[attachment]'=> '@' . GD_UPLOAD_FOLDER . $filename
	);

	$ch=curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	// Return a variable instead of posting it directly
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: multipart/form-data"));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, $AUTH_USERPWD);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	$result = curl_exec($ch);

	curl_close($ch);
	return $result;
}

function newBulletin($bbody, $bsubject, $btopic) {
	$response =
	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<bulletin>
	<body><![CDATA[$bbody]]></body>
	<subject>" . htmlspecialchars($bsubject, ENT_QUOTES, 'UTF-8') . "</subject>
	<topics type='array'>
	<topic>
	<code>$btopic</code>
	</topic>
	</topics>
	<open_tracking type='boolean'>false</open_tracking>
	<click_tracking type='boolean'>false</click_tracking>
	<categories type='array' />
	</bulletin>
	";

	return $response;
}

function testEmail($email) {
	$response =
	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<bulletin>
	<email_addresses type=\"array\">
    <email_address>$email</email_address>
    </email_addresses>
	</bulletin>
	";

	return $response;
}

function gd_create_bulletin($bbody, $bsubject, $btopic, $filename) {

	$url = 'bulletins/send_now.xml';

	if ($filename == '')
	{
		$result = http_post_xml($url,newBulletin($bbody, $bsubject, $btopic));
	}
	else
	{
		upload_file();
		return http_post_with_attachment($url,$bbody, $bsubject, $btopic, $filename);
	}

	return $result;
}

function gd_create_draft($bbody, $bsubject, $btopic, $filename) {

	$result = http_post_xml('bulletins.xml',newBulletin($bbody, $bsubject, $btopic));

	return $result;
}

function gd_post_file($draft_id, $filename){
	upload_file();
	$result = http_post_file('bulletins/' . $draft_id . '/files',$filename);

	return $result;
}

function gd_delete_attachment($draft_id, $attachment_id) {

	$result = http_delete('bulletins/' . $draft_id . '/files/' . $attachment_id);

	return $result;
}

function gd_send_draft($draft_id, $bbody, $bsubject, $btopic) {
	return http_post_xml('bulletins/' . $draft_id . '/send_now',newBulletin($bbody, $bsubject, $btopic));
}

function gd_update_draft($draft_id, $bbody, $bsubject, $btopic, $filename) {

	$result = http_put_xml('bulletins/' . $draft_id . '.xml',newBulletin($bbody, $bsubject, $btopic));

	return $result;
}

function gd_send_test_email($draft_id, $email) {

	$result = http_put_xml('bulletins/' . $draft_id . '/sendtest',testEmail($email));

	return $result;
}

function gd_get_bulletin_details_xml($bulletin_id) {
	return http_get('bulletins/' . $bulletin_id);
}

function upload_file() {
	/* $allowedExts = array("gif", "jpeg", "jpg", "png", "txt");
	   $temp = explode(".", $_FILES["file"]["name"]);
	   $extension = end($temp);
	if ((($_FILES["file"]["type"] == "image/gif")
			|| ($_FILES["file"]["type"] == "image/jpeg")
			|| ($_FILES["file"]["type"] == "image/jpg")
			|| ($_FILES["file"]["type"] == "image/pjpeg")
			|| ($_FILES["file"]["type"] == "image/x-png")
			|| ($_FILES["file"]["type"] == "image/png")
			|| ($_FILES["file"]["type"] == "text/plain"))
			&& ($_FILES["file"]["size"] < 200000)
			&& in_array($extension, $allowedExts))
	{ */
		if ($_FILES["file"]["error"] > 0)
		{
			//echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
		}
		else
		{
			/* echo "Upload: " . $_FILES["file"]["name"] . "<br>";
			echo "Type: " . $_FILES["file"]["type"] . "<br>";
			echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
			echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>"; */

			if (file_exists(GD_UPLOAD_FOLDER . $_FILES["file"]["name"]))
			{
				//echo $_FILES["file"]["name"] . " already exists. ";
			}
			else
			{
				move_uploaded_file($_FILES["file"]["tmp_name"],
				GD_UPLOAD_FOLDER . $_FILES["file"]["name"]);
				//echo "Stored in: " . GD_UPLOAD_FOLDER . $_FILES["file"]["name"];
			}
		}
	/* }
	else
	{
		echo "Invalid file";
	} */
}

?>
