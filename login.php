<?php

require('lib/MLM.php');

$success="true";	//5/9/2014 - PALS handling of 'false' not working. An invalid token string causes the desired result.
$token = "Login failed";

if ( isset($_REQUEST['user']) && 
     isset($_REQUEST['pass']) &&
     MLM::login($_REQUEST['user'], $_REQUEST['pass']) ) {
    $success = "true";
    $token = MLM::getToken();
}

?><login-token success="<?php echo $success . '">' . $token; ?></login-token>
