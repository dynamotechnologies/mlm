<?php
require_once("lib/MLM.php");
if ($_GET['action'] !== 'sync') { 
    $user = "mhsu02";
    $pass = md5("${user}MLMLOGIN");
    $token = "";
    if (MLM::login($user, $pass)) {
        $token = MLM::getToken();
    }
    header("Location: sync-categories.php?token=$token&action=sync");
    echo "Sync request login\n";
} else {
    try {
        echo "Sync Categories request\n";
        $mlm = new MLM();
        $mlm->syncCategories();
    } catch (Exception $e) {
        echo "Error during MLM category sync job, check configs!\n";
        echo $e->getMessage();
    }
}
?>
