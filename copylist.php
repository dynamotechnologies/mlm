<?php
    require('lib/MLM.php');
    require('lib/InputUtil.php');
    $mlm = new MLM();

    if (InputUtil::isGetIntSafe('id')) {
        $id = $_GET['id'];
        $list = $mlm->getList($id);
        $name = $list->name;
    } else if (InputUtil::postString('mlmaction')=='copy' &&
                InputUtil::isPostIntSafe('id')) {
        $fromId = $_POST['id'];
        $inputlists = InputUtil::postArray('lists');
        foreach ($inputlists as $toListId) {
            $mlm->copySubscribers($fromId, $toListId);
        }
        header("Location: copylist.php?id=$fromId");
    } else {
        echo "<p>List not found.</p>";
        exit(1);
    }

    echo $mlm->getStdPageBegin('Copy List Subscribers');

    echo "<h3>$name</h3>";
    echo '<a href="listhome.php?id=' . $id . '">&lt; Manage this list</a><br/>';

    echo "<p>All subscribers to the <strong>$name</strong> list will be copied to the lists you select below. Category subscribers will not be copied.</p>";
?>
<br/>
<form enctype="multipart/form-data" action="copylist.php" method="post">
<input type="hidden" name="mlmaction" value="copy"/>
<input name="id" type="hidden" value="<?php echo $id; ?>"/>
<input type="submit" id="submit" value="Copy Subscribers"/>
<br/><br/>
<?php
    echo '<table class="list1"><tr>' .
            '<td class="listleft"><strong>Mailing List</strong></td>' .
            '<td class="listright"><strong>Subscriber Count</strong></td>' .
        '</tr></table>';
    $toggler = false;
    foreach ($mlm->getLists() as $l) {
        // don't show the source list
        if ($l->id==$id)
            continue;

        $liststyle = ($toggler) ? "list1" : "list0";
        $subcount = sizeof($mlm->getSubscribersByList($l->id));

        $name = $l->name;
        $projectid = MLM::getProjectIdFromTopicId($l->topicid);
        if (strlen($projectid)>0) {
            $name .= ' (' . $projectid . ')';
        }

        echo '<table class="'.$liststyle.'"><tr>' .
                '<td class="listleft"><input type="checkbox" name="lists[]" value="' .
                    $l->id . '"/> ' . $name . '</td>' .
                '<td class="listright">' . $subcount . '</td>' .
            '</tr></table>';
        $toggler = !$toggler;
    }
?>
<br/>
<input type="submit" id="submit" value="Copy Subscribers"/>

<?php

    echo $mlm->getStdPageEnd();
?>
